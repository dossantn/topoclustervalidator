#ifndef TRIGCALORECSERVICES_TRIGCALOACCELERATIONEDM_H
#define TRIGCALORECSERVICES_TRIGCALOACCELERATIONEDM_H

#include <cmath>
#include <fstream>
#include <type_traits>
#include <filesystem>
#include <string>
#include <map>

#define MAXNEIGHBOURS 26
#define NCALOCELLS    187652
#define MAXCLUSTERS   100000

static const size_t NAccelTimers = 4; //number of AccelerationSVC timers
static const size_t NClustTimers = 7; //number of Cluster worker timers


typedef struct CellsDataToAccelrator_t{
public:
  float energy[NCALOCELLS]; // E for each cell
} CellsDataToAccelrator;


typedef struct CellsGeoData_t {
public:
  float eta[NCALOCELLS]; 
  float phi[NCALOCELLS]; 
  char  layer[NCALOCELLS];
} CellsGeoData;


typedef struct CellsNoiseData_t {
public:
  float threshold[NCALOCELLS]; 
} CellsNoiseData;


//In use: ----------------------------------------------------------------------
typedef struct TimeStamp{
public:
  TimeStamp(int e, long s, long ns) : m_event(e), m_sec(s), m_nsec(ns) {};
  TimeStamp(const TimeStamp& ts) : m_event(ts.m_event), m_sec(ts.m_sec), m_nsec(ts.m_nsec) {};
  int m_event;
  long m_sec, m_nsec;
} TIME_STAMP;

typedef struct CellsFullData_t {
public:
  float energy[NCALOCELLS]; // E for each cell
} CELLSFULLDATA;

typedef struct FullCaloGeometry_t {
public:
  int    caloSample[NCALOCELLS];
  float  x[NCALOCELLS];
  float  y[NCALOCELLS];
  float  z[NCALOCELLS];
  float  eta[NCALOCELLS]; 
  float  phi[NCALOCELLS]; 
  float  noise[NCALOCELLS];
  unsigned int nNeighbours[NCALOCELLS];               //Number of neighbours
  unsigned int neighbours[NCALOCELLS][MAXNEIGHBOURS]; //MAXNEIGHBOURS = 26
} FULLCALOGEOMETRY;

typedef struct CaloCellPairs_t {
public:
  unsigned int cellID;
  unsigned int neighbourID;
} CALOCELLPAIRS;

typedef struct SeedCells_t {
public:
  unsigned int cellID;
  float cellSN2;
} SEEDCELLS;

typedef struct ProtoCluster_t {
public:
  unsigned int tag;
  float Energy;
} PROTOCLUSTER;

typedef struct SimpleStatusCode_t {
public:
  bool m_Success;
} SIMPLE_STATUSCODE;

typedef struct ProtoClusterCells_t {
public:
  float cellSN2[NCALOCELLS]; //stores the signal to noise (SN) value (also seed, grow, border information)
  int   seedTag[NCALOCELLS]; //cluster tag, position of the seed in the sorted S/N seeds list (lower first)  (-1 if out of cluster)
} PCLUSTER_CELLS;

typedef struct acceleratorFullClusters_t {
public:
  int nClusters;                  // number of clusters
  int seedTag[NCALOCELLS];
  int clusterSize[MAXCLUSTERS];
//  int clusterSizeSum[50000];      // number of cells in each cluster + cells in previous clusters (gives the location of the cells in the cluster [ clusterSizeSum[i-1]; clusterSizeSum[i]-1 ])
//  int cellsInCluster[NCALOCELLS]; // cells in each cluster
  float cellSN2[NCALOCELLS];      //stores the signal to noise (SN) square value
  float clusterEnergy[MAXCLUSTERS];
  float clusterEt[MAXCLUSTERS];
  float clusterEta[MAXCLUSTERS];
  float clusterPhi[MAXCLUSTERS];
  float timers[NClustTimers];     //Timers for cluster worker, cluster kernels and cluster memcopy; see CaloClusterMakerWorker.cxx for details
} FULLCLUSTERS;

//for debug
typedef struct debugClusters_t {
public:
  int   seedTag[NCALOCELLS];
  //float cellNoise[NCALOCELLS];
  //float cellSN2[NCALOCELLS];
  float cellE[NCALOCELLS];
  
  int nClusters;        // number of clusters
  int clusterNCells[50000];
  float clusterEnergy[50000];
  float clusterEt[50000];
  float clusterEta[50000];
  float clusterPhi[50000];
} DEBUGFULLCLUSTERS;
//------------------------------------------------------------------------------


typedef struct SimpleOutputData {
public:
  int m_CellsPasses;
} SIMPLE_OUTPUT_DATA;

typedef struct FullCaloNoise_t {
public:
  float noise[NCALOCELLS];
} FULLCALONOISE;


inline bool sortCaloCellRatio(const int &cell1Tag, const int &cell2Tag) {return cell1Tag>cell2Tag;}


static constexpr char * regNames[3] = {"central","end-cap","forward"};
static constexpr float regCuts[3] = {1.5, 3.2, 5};

static constexpr float SNR_thresholds[3] = {0., 2., 4.};

template <class T>
constexpr T pi = T{3.1415926535897932384626433832795028841971693993751058209749445923078164\
06286208998628034825342117067982148086513282306647093844609550582231725359408128481117450284102701938521105\
55964462294895493038196442881097566593344612847564823378678316527120190914564856692346034861045432664821339\
36072602491412737245870066063155881748815209209628292540917153643678925903600113305305488204665213841469519\
415116094330572703657595919530921861173819326117931051185480744623799627495673518857527248912279381830119491298336733624L};

template <class T>
T wrapPhi( const T phi )
{
  return phi;
  /*
  const T comp = phi / pi<T>;
  T ret = std::fmod(comp + 1, 2);
  if (ret < 0)
    {
      ret += 2;
    }
    return (ret - 1) * pi<T>;*/
}

template <class T1, class T2>
decltype(std::declval<T1>() - std::declval<T2>()) minDiffPhi(const T1 phi1, const T2 phi2)
{ 
  using T = decltype(phi1 - phi2);

  const T phi_r1 = wrapPhi<T>(phi1);
  const T phi_r2 = wrapPhi<T>(phi2);

  return wrapPhi<T>(phi_r1 - phi_r2);
}

template <class Tb, class Ta>
inline Tb proxim_ath(const Tb b, const Ta a)
{
  const Ta aplus = a + pi<Ta>;
  const Ta aminus = a - pi<Ta>;
  Tb ret = b;
  if (b > aplus) {
    do {
      ret -= 2*pi<Tb>;
    } while(ret > aplus);
  }
  else if (b < aminus) {
    do {
      ret += 2*pi<Tb>;
    } while(ret < aminus);
  }
  return ret;
}


template <class T>
bool load_from_binary_file(const std::filesystem::path& file_name, T & object)
{
  static_assert(std::is_trivially_copyable_v<T>, "The class must be trivially copyable for binary input to work!");
  const auto file_size = std::filesystem::file_size(file_name);

  if (file_size < sizeof(T))
    {
      std::cout << "ERROR: File '" << file_name << "' is smaller than the desired object (" << file_size << " vs " << sizeof(T) << ")!" << std::endl;
      return true;
    }
  
  const bool has_a_bool_at_beginning = (file_size == sizeof(T) + sizeof(bool));
  //For compatibility with my way of outputting.

  std::ifstream in(file_name, std::ios::binary);
  if (!in.is_open())
    {
      std::cout << "ERROR: Can't open file '" << file_name << "'" << std::endl;
      return true;
    }
  if (has_a_bool_at_beginning)
    {
      bool has_object;
      in.read((char *) &has_object, sizeof(bool));
      if (!has_object)
	{
	  std::cout << "WARNING: File '" << file_name << "' appears to hold no object, but we'll try reading nonetheless..." << std::endl;
	}
    }
  in.read((char *) &object, sizeof(T));
  if (in.fail())
    {
      std::cout << "ERROR: Problem reading from file '" << file_name << "'" << std::endl;
      in.close();
      return true;
    }
  else
    {
      in.close();
      return false;
    }
}


template <class T>
bool load_from_abbreviated_file(const std::filesystem::path& file_name, T & object)
{
  static_assert(std::is_trivially_copyable_v<T>, "The class must be trivially copyable for binary input to work!");
  const auto file_size = std::filesystem::file_size(file_name);

  std::ifstream in(file_name, std::ios::binary);
  if (!in.is_open())
    {
      std::cout << "ERROR: Can't open file '" << file_name << "'" << std::endl;
      return true;
    }

  if constexpr (std::is_same_v<T, DEBUGFULLCLUSTERS>)
    {
       in.read((char *) object.seedTag, sizeof(int) * NCALOCELLS);
       in.read((char *) object.cellE, sizeof(float) * NCALOCELLS);
       in.read((char *) &(object.nClusters), sizeof(int));
       if (object.nClusters < 0 || object.nClusters > 50000)
	 {
	   std::cout << "ERROR: cluster size " << object.nClusters << " is out of bounds. Is '" << file_name << "' the correct file?" << std::endl;
	   return true;
	 }
       in.read((char *) object.clusterNCells, sizeof(int) * object.nClusters);
       in.read((char *) object.clusterEnergy, sizeof(float) * object.nClusters);
       in.read((char *) object.clusterEt, sizeof(float) * object.nClusters);
       in.read((char *) object.clusterEta, sizeof(float) * object.nClusters);
       in.read((char *) object.clusterPhi, sizeof(float) * object.nClusters);
    }
  else
    {
      std::cout << "ERROR: Class " << typeid(object).name() << " not compatible with abbreviated I/O!" << std::endl;
      return true;
    }

  if (in.fail())
    {
      std::cout << "ERROR: Problem reading from file '" << file_name << "'" << std::endl;
      in.close();
      return true;
    }
  else
    {
      in.close();
      return false;
    }
}



template <class T>
bool load_from_file(const std::filesystem::path& file_name, T & object)
{
#if DEBUG_MODE
  std::cout << "Going to load '" << file_name << "' to find an " << typeid(T).name() << "." << std::endl;
#endif
  if (file_name.extension() == ".diag")
    {
      return load_from_binary_file(file_name, object);
    }
  else if (file_name.extension() == ".abrv")
    {
      return load_from_abbreviated_file(file_name, object);
    }  
  else
    {
      std::cout << "ERROR: Unrecognized extension '" << file_name.extension() << "'." << std::endl;
      return true;
    }
}

void setup_files(const std::string& path_to_dir, const std::string& desired_type, std::map<unsigned int, std::filesystem::path>& map)
{
  
#if DEBUG_MODE
  std::cout << "Setting up files in '" << path_to_dir << "' of the form '*" << desired_type << "*." << desired_extension << "'." << std::endl;
#endif

  map.clear();
  
  for (auto& entry : std::filesystem::directory_iterator(path_to_dir))
    {
#if DEBUG_MODE
      std::cout << entry.path() << std::endl;
#endif
      if (desired_type.size() > 0 && entry.path().string().find(desired_type) == std::string::npos)
        {
          //Isn't of the desired type.
          //An empty desired_type accepts anything that's not "Maker" or "Splitter".
          //(TODO: Remove hardcoding?)
          continue;
        }
      else if (desired_type.size() == 0 && (entry.path().string().find("Maker") != std::string::npos ||
                                            entry.path().string().find("Splitter") != std::string::npos))
        {
          continue;
        }
      const std::string name = entry.path().stem().string();
      if (name == "geometryData")
	{
	  continue;
	}
      const auto before_digits = name.find_last_of('_');
      if (before_digits == std::string::npos)
        {
          std::cout << "WARNING: File name '" << name << "' seems invalid. Skipping it for now, please check." << std::endl;
        }
      else
        {
          const auto val = stoull(name.substr(before_digits+1));
          map[val] = entry.path();
        }
    }
}
  


#endif
