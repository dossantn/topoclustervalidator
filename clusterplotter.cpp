#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <map>
#include <string>
#include <cstring>
#include <fstream>
#include "GeneralDefines.h"

#include <filesystem>
//We can use C++17,
//no need for external libraries!

#include "ClusterPlotter.h"

struct PlotDefs
{
  std::string name;
  std::string pref;
  std::string suf;
  double xmin, xmax, ymin, ymax;
  int bin_size;
  double labelx, labely;

  PlotDefs(const std::string &n = "", const std::string &p = "", const std::string & s = "",
	   const double xmi = -1, const double xma = -1, const double ymi = -1, const double yma = -1,
	   const int bs = -1, const double lx = -1, const double ly = -1):
    name(n), pref(p), suf(s), xmin(xmi), xmax(xma), ymin(ymi), ymax(yma), bin_size(bs), labelx(lx), labely(ly)
  {
  }

  void reset()
  {
    name = "";
    pref = "";
    suf = "";
    xmin = -1;
    xmax = -1;
    ymin = -1;
    ymax = -1;
    bin_size = -1;
    labelx = -1;
    labely = -1;
  }

  template <class Stream> friend
  Stream & operator << (Stream &str, const PlotDefs &pd)
  {
    str << pd.name << "|" << pd.pref << "|" << pd.suf << "|" << pd.xmin << " " << pd.xmax << " " << pd.ymin << " " << pd.ymax << " " << pd.bin_size << " " << pd.labelx << " " << pd.labely << std::endl;
    return str;
  }

  template <class Stream> friend
  Stream& operator>> (Stream& str, PlotDefs &pd)
  { 
    pd.reset();

    std::string line;
    std::getline(str, line);

    const size_t pos_1 = line.find('|');
    if (pos_1 == std::string::npos)
      {
	return str;
      }
    pd.name = line.substr(0, pos_1);

    size_t pos_final = pos_1;

    const size_t pos_2 = line.find('|', pos_1 + 1);


    if (pos_2 != std::string::npos)
      {
	pd.pref = line.substr(pos_1 + 1, pos_2 - pos_1 - 1);
	pos_final = pos_2;
	
	const size_t pos_3 = line.find('|', pos_2 + 1);
	if (pos_3 != std::string::npos)
	  {
	    pd.suf = line.substr(pos_2 + 1, pos_3 - pos_2 - 1);
	    pos_final = pos_3;
	  }
      }
    
    std::stringstream readstr(line.substr(pos_final+1));

    readstr >> pd.xmin >> pd.xmax >> pd.ymin >> pd.ymax >> pd.bin_size >> pd.labelx >> pd.labely;
  
    return str;
  }
};



struct ProgArgs
{
  std::vector<std::string> in_paths;
  std::vector<std::string> names;
  std::string out_path;
  double min_similarity;
  double term_weight;
  double grow_weight;
  double seed_weight;
  bool place_titles;
  std::string plot_label;
  std::vector<PlotDefs> plot_reqs;
  std::vector<PlotDefs> togetherplot_reqs;
  bool normalize_hists;
  bool skip_almost_everything;
  bool use_ATLAS_style;
};

void read_plots_file(const std::filesystem::path &file, std::vector<PlotDefs>& vec)
{
  vec.clear();
  std::ifstream in(file);
  while (in.good())
    {
      if (in.peek() == '\n')
	{
	  in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}
      else if (in.peek() == '#')
	{
	  in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}
      else
	{
	  PlotDefs pd;
	  in >> pd;
	  if (pd.name != "")
	    {
	      vec.push_back(pd);
	    }
	}
    }
/*
  for (const auto & pd : vec)
    {
      std::cout << " READ: " << pd << std::endl;
      }
*/
}

void set_default_plots(ProgArgs& args)
{
  constexpr int default_num_bins = 250;
  constexpr int shorter_num_bins = 100;
  constexpr int small_num_bins = 50;
  constexpr int smallest_num_bins = 25;
  /*

    args.plot_reqs.emplace_back("diff_num_hist", "", "", -1, -1, -1, -1, smallest_num_bins);
    args.plot_reqs.emplace_back("diff_num_hist", "", "_zoom", -10, 10);
    args.plot_reqs.emplace_back("diff_num_rel_hist", "", "", -1, -1);
    args.plot_reqs.emplace_back("frac_cell_diff_reg_hist", "", "", -1, -1);
    args.plot_reqs.emplace_back("frac_cell_diff_type_hist", "", "", -1, -1);
    args.plot_reqs.emplace_back("frac_cell_diff_reg_hist", "", "_zoom", 0, 0.2);
    args.plot_reqs.emplace_back("frac_cell_diff_type_hist", "", "_zoom", 0, 0.2);

    args.plot_reqs.emplace_back("diff_cell_per_cluster", "", "", -1, -1, -1, -1, default_num_bins);
    args.plot_reqs.emplace_back("diff_cell_per_cluster", "", "_zoom1", 0, 2500);
    args.plot_reqs.emplace_back("diff_cell_per_cluster", "", "_zoom2", 0, 500);
    args.plot_reqs.emplace_back("diff_cell_per_cluster", "", "_zoom3", 0, 100);

    args.plot_reqs.emplace_back("rel_diff_cell_per_cluster", "", "", -1, -1);
    args.plot_reqs.emplace_back("rel_diff_cell_per_cluster", "", "_zoom1", 0, 0.5);
    args.plot_reqs.emplace_back("rel_diff_cell_per_cluster", "", "_zoom2", 0., 0.25);
    args.plot_reqs.emplace_back("rel_diff_cell_per_cluster", "", "_zoom3", 0., 0.05);

    args.plot_reqs.emplace_back("delta_R_hist", "", "", -1,  -1);
    args.plot_reqs.emplace_back("delta_R_hist", "", "_zoom1", 0.,  0.25);
    args.plot_reqs.emplace_back("delta_R_hist", "", "_zoom2", 0.,  0.001);
    args.plot_reqs.emplace_back("delta_R_hist", "", "_zoom3", 0.,  0.0001);
  */

  args.plot_reqs.emplace_back("delta_cells_vs_delta_R", "", "", -1, -1,-1,-1, default_num_bins);
  args.plot_reqs.emplace_back("delta_cells_vs_delta_R", "", "_zoom1", 0., 0.25,-1,-1);
  args.plot_reqs.emplace_back("delta_cells_vs_delta_R", "", "_zoom2", 0., 0.001,-1,-1);
  args.plot_reqs.emplace_back("delta_cells_vs_delta_R", "", "_zoom3", 0., 0.0001,-1,-1);

  args.plot_reqs.emplace_back("delta_R_vs_E", "", "", -1, -1,-1,-1);
  args.plot_reqs.emplace_back("delta_R_vs_E", "", "_zoom", -1000, 1000,0,0.15);
  args.plot_reqs.emplace_back("delta_R_vs_Et", "", "", -1, -1,-1,-1);
  args.plot_reqs.emplace_back("delta_R_vs_Et", "", "_zoom1", -175, 175,0,0.15);
  args.plot_reqs.emplace_back("delta_R_vs_Et", "", "_zoom2", -75, 75,0,0.15);
  args.plot_reqs.emplace_back("delta_R_vs_Eta", "", "", -1, -1,-1,-1);
  args.plot_reqs.emplace_back("delta_R_vs_Eta", "", "_zoom", -7.5, 7.5,0,0.15);
  args.plot_reqs.emplace_back("delta_R_vs_Phi", "", "", -1, -1,-1,-1);
  args.plot_reqs.emplace_back("delta_R_vs_Phi", "", "_zoom", -1, -1,0,0.15);
  args.plot_reqs.emplace_back("delta_R_vs_size", "", "", -1, -1,-1,-1);
  args.plot_reqs.emplace_back("delta_R_vs_size", "", "_zoom", 0, 3000,0.,0.15);

  args.plot_reqs.emplace_back("delta_Et_rel_vs_Et", "", "", -1, -1,-1.,10.);
  args.plot_reqs.emplace_back("delta_Et_rel_vs_Et", "", "_zoom1", -150, 150,-0.25,0.25);
  args.plot_reqs.emplace_back("delta_Et_rel_vs_Et", "", "_zoom2", -75, 75,-0.25,0.25);
  args.plot_reqs.emplace_back("delta_Et_rel_hist", "", "", -1, -1);
  args.plot_reqs.emplace_back("delta_Et_rel_hist", "", "_zoom", -0.5, 0.5);
  args.plot_reqs.emplace_back("Et1_vs_Et2", "", "", -1, -1,-1,-1);
  args.plot_reqs.emplace_back("Et1_vs_Et2", "", "_zoom", -100, 100,-100,100);
  args.plot_reqs.emplace_back("E1_vs_E2", "", "", -1, -1,-1,-1);
  args.plot_reqs.emplace_back("E1_vs_E2", "", "_zoom", -200, 200,-200,200);
  /*
    args.plot_reqs.emplace_back("Et_hist", "", "", -1, -1);
    args.plot_reqs.emplace_back("Et_hist", "", "_zoom1", -200, 200);
    args.plot_reqs.emplace_back("Et_hist", "", "_zoom2", -100, 100);
    args.plot_reqs.emplace_back("E_hist", "", "", -1, -1);
    args.plot_reqs.emplace_back("E_hist", "", "_zoom", -1500, 1500);
    args.plot_reqs.emplace_back("cluster_size_hist", "", "", -1, -1);
    args.plot_reqs.emplace_back("cluster_size_hist", "", "_zoom", 0, 1000);
    args.plot_reqs.emplace_back("cluster_eta_hist", "", "", -1, -1);
    args.plot_reqs.emplace_back("cluster_eta_hist", "", "_full", -7.5, 7.5);
    args.plot_reqs.emplace_back("cluster_eta_hist", "", "_zoom0", -50, 50);
    args.plot_reqs.emplace_back("cluster_eta_hist", "", "_zoom1", -10, 10);
    args.plot_reqs.emplace_back("cluster_eta_hist", "", "_zoom2", -3, 3);
    args.plot_reqs.emplace_back("cluster_phi_hist", "", "", -1, -1);
    args.plot_reqs.emplace_back("cluster_phi_hist", "", "_full", -pi<double>, pi<double>);


    args.plot_reqs.emplace_back("delta_eta_hist", "", "", -1,  -1);
    args.plot_reqs.emplace_back("delta_eta_hist", "", "_zoom1", -10,  10);
    args.plot_reqs.emplace_back("delta_eta_hist", "", "_zoom3", -1,  1);
    args.plot_reqs.emplace_back("delta_eta_hist", "", "_zoom3", -0.25,  0.25);
    args.plot_reqs.emplace_back("delta_eta_hist", "", "_zoom4", -0.1,  0.1);
    args.plot_reqs.emplace_back("delta_eta_hist", "", "_zoom5", -0.01,  0.01);
    args.plot_reqs.emplace_back("delta_eta_hist", "", "_zoom6", -0.001,  0.001);

    args.plot_reqs.emplace_back("delta_phi_hist", "", "", -1,  -1);
    args.plot_reqs.emplace_back("delta_phi_hist", "", "_full", -pi<double>,  pi<double>);

    args.plot_reqs.emplace_back("delta_E_hist", "", "", -1,  -1);
    args.plot_reqs.emplace_back("delta_E_hist", "", "_zoom1", -1000,  1000);
    args.plot_reqs.emplace_back("delta_E_hist", "", "_zoom2", -500,  500);
    args.plot_reqs.emplace_back("delta_E_hist", "", "_zoom3", -100,  100);
    args.plot_reqs.emplace_back("delta_E_hist", "", "_zoom4", -50,  50);
    args.plot_reqs.emplace_back("delta_E_hist", "", "_zoom5", -10,  10);
    args.plot_reqs.emplace_back("delta_E_hist", "", "_zoom6", -1,  1);
    args.plot_reqs.emplace_back("delta_E_hist", "", "_zoom7", -0.1,  0.1);
    args.plot_reqs.emplace_back("delta_E_hist", "", "_zoom8", -0.01,  0.01);
    args.plot_reqs.emplace_back("delta_E_hist", "", "_zoom9", -0.001,  0.001);

    args.plot_reqs.emplace_back("delta_Et_hist", "", "", -1,  -1);
    args.plot_reqs.emplace_back("delta_Et_hist", "", "_zoom1", -1000,  1000);
    args.plot_reqs.emplace_back("delta_Et_hist", "", "_zoom2", -500,  500);
    args.plot_reqs.emplace_back("delta_Et_hist", "", "_zoom3", -100,  100);
    args.plot_reqs.emplace_back("delta_Et_hist", "", "_zoom4", -50,  50);
    args.plot_reqs.emplace_back("delta_Et_hist", "", "_zoom5", -10,  10);
    args.plot_reqs.emplace_back("delta_Et_hist", "", "_zoom6", -1,  1);
    args.plot_reqs.emplace_back("delta_Et_hist", "", "_zoom7", -0.1,  0.1);
    args.plot_reqs.emplace_back("delta_Et_hist", "", "_zoom8", -0.01,  0.01);
    args.plot_reqs.emplace_back("delta_Et_hist", "", "_zoom9", -0.001,  0.001);



    args.plot_reqs.emplace_back("cluster_number_hist", "", "", -1, -1, -1, -1, smallest_num_bins);
    args.plot_reqs.emplace_back("term_cell_E_perc_hist", "", "", -1, -1);
  */

  args.plot_reqs.emplace_back("term_cell_E_perc_vs_E_ref", "", "", -1, -1,-1,-1, default_num_bins);
  args.plot_reqs.emplace_back("term_cell_E_perc_vs_E_test", "", "", -1, -1,-1,-1);

  /*


    args.plot_reqs.emplace_back("unmatched_number_hist", "", "", -1, -1, -1, -1, smallest_num_bins);


    args.plot_reqs.emplace_back("unmatched_perc_hist", "", "", -1, -1, -1, -1, small_num_bins);


    args.plot_reqs.emplace_back("unmatched_E_hist", "", "", -1, -1, -1, -1,  shorter_num_bins);
    args.plot_reqs.emplace_back("unmatched_E_hist", "", "_zoom", -50, 100);
    args.plot_reqs.emplace_back("unmatched_Et_hist", "", "", -1, -1);
    args.plot_reqs.emplace_back("unmatched_Et_hist", "", "_zoom1", -50, 50);
    args.plot_reqs.emplace_back("unmatched_Et_hist", "", "_zoom2", -20, 20);
    args.plot_reqs.emplace_back("unmatched_sizes_hist", "", "", -1, -1);
    args.plot_reqs.emplace_back("unmatched_sizes_hist", "", "_zoom", 0, 1000);
    args.plot_reqs.emplace_back("unmatched_eta_hist", "", "", -1, -1);
    args.plot_reqs.emplace_back("unmatched_eta_hist", "", "_full", -7.5, 7.5);
    args.plot_reqs.emplace_back("unmatched_eta_hist", "", "_zoom", -3, 3);
    args.plot_reqs.emplace_back("unmatched_phi_hist", "", "", -1, -1);
    args.plot_reqs.emplace_back("unmatched_phi_hist", "", "_full", -pi<double>, pi<double>);

    args.plot_reqs.emplace_back("equal_cells_delta_R", "", "", -1,  -1, -1, -1, default_num_bins);
    args.plot_reqs.emplace_back("equal_cells_delta_R", "", "_zoom1", 0.,  0.25);
    args.plot_reqs.emplace_back("equal_cells_delta_R", "", "_zoom2", 0.,  0.001);
    args.plot_reqs.emplace_back("equal_cells_delta_R", "", "_zoom3", 0.,  0.0001);


    args.plot_reqs.emplace_back("equal_cells_delta_eta", "", "", -1,  -1);
    args.plot_reqs.emplace_back("equal_cells_delta_eta", "", "_zoom1", -10,  10);
    args.plot_reqs.emplace_back("equal_cells_delta_eta", "", "_zoom2", -1,  1);
    args.plot_reqs.emplace_back("equal_cells_delta_eta", "", "_zoom3", -0.25,  0.25);
    args.plot_reqs.emplace_back("equal_cells_delta_eta", "", "_zoom4", -0.1,  0.1);
    args.plot_reqs.emplace_back("equal_cells_delta_eta", "", "_zoom5", -0.01,  0.01);
    args.plot_reqs.emplace_back("equal_cells_delta_eta", "", "_zoom6", -0.001,  0.001);


    args.plot_reqs.emplace_back("equal_cells_delta_phi", "", "", -1,  -1);
    args.plot_reqs.emplace_back("equal_cells_delta_phi", "", "_full", -pi<double>,  pi<double>);
    args.plot_reqs.emplace_back("equal_cells_delta_phi", "", "_zoom1", -1,  1);
    args.plot_reqs.emplace_back("equal_cells_delta_phi", "", "_zoom2", -0.1,  0.1);

    args.plot_reqs.emplace_back("equal_cells_delta_E", "", "", -1,  -1);
    args.plot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom1", -1000,  1000);
    args.plot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom2", -500,  500);
    args.plot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom3", -100,  100);
    args.plot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom4", -50,  50);
    args.plot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom5", -10,  10);
    args.plot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom6", -1,  1);
    args.plot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom7", -0.1,  0.1);
    args.plot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom8", -0.01,  0.01);
    args.plot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom9", -0.001,  0.001);

    args.plot_reqs.emplace_back("equal_cells_delta_E_rel", "", "", -1,  -1);
    args.plot_reqs.emplace_back("equal_cells_delta_E_rel", "", "_zoom1", -1,  1);
    args.plot_reqs.emplace_back("equal_cells_delta_E_rel", "", "_zoom2", -0.5,  0.5);
    args.plot_reqs.emplace_back("equal_cells_delta_E_rel", "", "_zoom3", -0.1,  0.1);

    args.plot_reqs.emplace_back("equal_cells_delta_Et", "", "", -1,  -1);
    args.plot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom1", -1000,  1000);
    args.plot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom2", -500,  500);
    args.plot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom3", -100,  100);
    args.plot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom4", -50,  50);
    args.plot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom5", -10,  10);
    args.plot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom6", -1,  1);
    args.plot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom7", -0.1,  0.1);
    args.plot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom8", -0.01,  0.01);
    args.plot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom9", -0.001,  0.001);

    args.plot_reqs.emplace_back("equal_cells_delta_Et_rel", "", "", -1,  -1);
    args.plot_reqs.emplace_back("equal_cells_delta_Et_rel", "", "_zoom1", -1,  1);
    args.plot_reqs.emplace_back("equal_cells_delta_Et_rel", "", "_zoom2", -0.5,  0.5);
    args.plot_reqs.emplace_back("equal_cells_delta_Et_rel", "", "_zoom3", -0.1,  0.1);
  */
  /*

    args.plot_reqs.emplace_back("post_calc_delta_R", "", "", -1,  -1);
    args.plot_reqs.emplace_back("post_calc_delta_R", "", "_zoom1", 0.,  0.25);
    args.plot_reqs.emplace_back("post_calc_delta_R", "", "_zoom2", 0.,  0.001);
    args.plot_reqs.emplace_back("post_calc_delta_R", "", "_zoom3", 0.,  0.0001);


    args.plot_reqs.emplace_back("post_calc_delta_eta", "", "", -1,  -1);
    args.plot_reqs.emplace_back("post_calc_delta_eta", "", "_zoom1", -10,  10);
    args.plot_reqs.emplace_back("post_calc_delta_eta", "", "_zoom2", -1,  1);
    args.plot_reqs.emplace_back("post_calc_delta_eta", "", "_zoom3", -0.25,  0.25);
    args.plot_reqs.emplace_back("post_calc_delta_eta", "", "_zoom4", -0.1,  0.1);
    args.plot_reqs.emplace_back("post_calc_delta_eta", "", "_zoom5", -0.01,  0.01);


    args.plot_reqs.emplace_back("post_calc_delta_phi", "", "", -1,  -1);
    args.plot_reqs.emplace_back("post_calc_delta_phi", "", "_full", -pi<double>,  pi<double>);
    args.plot_reqs.emplace_back("post_calc_delta_phi", "", "_zoom1", -1,  1);
    args.plot_reqs.emplace_back("post_calc_delta_phi", "", "_zoom2", -0.1,  0.1);

    args.plot_reqs.emplace_back("post_calc_delta_E", "", "", -1,  -1);
    args.plot_reqs.emplace_back("post_calc_delta_E", "", "_zoom1", -1000,  1000);
    args.plot_reqs.emplace_back("post_calc_delta_E", "", "_zoom2", -500,  500);
    args.plot_reqs.emplace_back("post_calc_delta_E", "", "_zoom3", -100,  100);
    args.plot_reqs.emplace_back("post_calc_delta_E", "", "_zoom4", -50,  50);
    args.plot_reqs.emplace_back("post_calc_delta_E", "", "_zoom5", -10,  10);
    args.plot_reqs.emplace_back("post_calc_delta_E", "", "_zoom6", -1,  1);
    args.plot_reqs.emplace_back("post_calc_delta_E", "", "_zoom7", -0.1,  0.1);
    args.plot_reqs.emplace_back("post_calc_delta_E", "", "_zoom8", -0.01,  0.01);
    args.plot_reqs.emplace_back("post_calc_delta_E", "", "_zoom9", -0.001,  0.001);

    args.plot_reqs.emplace_back("post_calc_delta_E_rel", "", "", -1,  -1);
    args.plot_reqs.emplace_back("post_calc_delta_E_rel", "", "_zoom1", -1,  1);
    args.plot_reqs.emplace_back("post_calc_delta_E_rel", "", "_zoom2", -0.5,  0.5);
    args.plot_reqs.emplace_back("post_calc_delta_E_rel", "", "_zoom3", -0.1,  0.1);
    args.plot_reqs.emplace_back("post_calc_delta_E_rel", "", "_zoom4", -0.005,  0.005);


    args.plot_reqs.emplace_back("delta_post_E", "", "", -1,  -1);
    args.plot_reqs.emplace_back("delta_post_E", "", "_zoom1", -1000,  1000);
    args.plot_reqs.emplace_back("delta_post_E", "", "_zoom2", -500,  500);
    args.plot_reqs.emplace_back("delta_post_E", "", "_zoom3", -100,  100);
    args.plot_reqs.emplace_back("delta_post_E", "", "_zoom4", -50,  50);
    args.plot_reqs.emplace_back("delta_post_E", "", "_zoom5", -10,  10);
    args.plot_reqs.emplace_back("delta_post_E", "", "_zoom6", -1,  1);


    args.plot_reqs.emplace_back("delta_post_E_rel", "", "", -1,  -1);
    args.plot_reqs.emplace_back("delta_post_E_rel", "", "_zoom1", -1,  1);
    args.plot_reqs.emplace_back("delta_post_E_rel", "", "_zoom2", -0.5,  0.5);
    args.plot_reqs.emplace_back("delta_post_E_rel", "", "_zoom3", -0.1,  0.1);
    args.plot_reqs.emplace_back("delta_post_E_rel", "", "_zoom4", -0.005,  0.005);

    args.plot_reqs.emplace_back("delta_post_eta", "", "", -1,  -1);
    args.plot_reqs.emplace_back("delta_post_eta", "", "_zoom1", -10,  10);
    args.plot_reqs.emplace_back("delta_post_eta", "", "_zoom2", -1,  1);
    args.plot_reqs.emplace_back("delta_post_eta", "", "_zoom3", -0.25,  0.25);
    args.plot_reqs.emplace_back("delta_post_eta", "", "_zoom4", -0.1,  0.1);


    args.plot_reqs.emplace_back("delta_post_phi", "", "", -1,  -1);
    args.plot_reqs.emplace_back("delta_post_phi", "", "_full", -pi<double>,  pi<double>);
    args.plot_reqs.emplace_back("delta_post_phi", "", "_zoom1", -1,  1);
    args.plot_reqs.emplace_back("delta_post_phi", "", "_zoom2", -0.1,  0.1);

  */



  args.plot_reqs.emplace_back("time_histogram", "", "",  -1,  -1, small_num_bins);
  args.plot_reqs.emplace_back("time_histogram", "", "_zoom1",  0,  60000);
  args.plot_reqs.emplace_back("time_histogram", "", "_zoom2",  5000,  60000);

  args.plot_reqs.emplace_back("time_ratio_histogram", "", "",  -1,  -1);
  args.plot_reqs.emplace_back("time_ratio_histogram", "", "_zoom1",  0,  5);
  args.plot_reqs.emplace_back("time_ratio_histogram", "", "_zoom2",  0,  2);
  args.plot_reqs.emplace_back("time_ratio_histogram", "", "_zoom3",  0,  1);
  args.plot_reqs.emplace_back("time_ratio_histogram", "", "_zoom4",  0,  0.75);
  args.plot_reqs.emplace_back("time_ratio_histogram", "", "_zoom5",  0,  0.5);

  args.plot_reqs.emplace_back("time_calc_gpu", "", "",  -1,  -1);


    
  args.togetherplot_reqs.emplace_back("cluster_number_hist", "", "", -1, -1, -1, -1, smallest_num_bins);
  args.togetherplot_reqs.emplace_back("cluster_number_hist", "", "_zoom1", 0, 1800);
  args.togetherplot_reqs.emplace_back("diff_num_hist", "", "", -1, -1);

  args.togetherplot_reqs.emplace_back("diff_num_hist", "", "_zoom1", -10.5, 10.5, -1, -1, 21);

  args.togetherplot_reqs.emplace_back("unmatched_number_hist", "", "", -0.5,  5.5, -1, -1, 6);

  args.togetherplot_reqs.emplace_back("time_histogram", "", "",  -1,  -1, -1, -1, small_num_bins);
  args.togetherplot_reqs.emplace_back("time_histogram", "", "_zoom1",  0,  60000);
  args.togetherplot_reqs.emplace_back("time_histogram", "", "_zoom2",  5000,  60000);

  args.togetherplot_reqs.emplace_back("time_ratio_histogram", "", "",  -1,  -1);
  args.togetherplot_reqs.emplace_back("time_ratio_histogram", "", "_zoom1",  0,  5);
  args.togetherplot_reqs.emplace_back("time_ratio_histogram", "", "_zoom2",  0,  2);
  args.togetherplot_reqs.emplace_back("time_ratio_histogram", "", "_zoom3",  0,  1);
  args.togetherplot_reqs.emplace_back("time_ratio_histogram", "", "_zoom4",  0,  0.75);
  args.togetherplot_reqs.emplace_back("time_ratio_histogram", "", "_zoom5",  0,  0.5);


  args.togetherplot_reqs.emplace_back("time_invratio_histogram", "", "",  -1,  -1);
  args.togetherplot_reqs.emplace_back("time_invratio_histogram", "", "_zoom1",  0,  25);
  args.togetherplot_reqs.emplace_back("time_invratio_histogram", "", "_zoom2",  0,  20);
  args.togetherplot_reqs.emplace_back("time_invratio_histogram", "", "_zoom3",  0,  15);
  args.togetherplot_reqs.emplace_back("time_invratio_histogram", "", "_zoom4",  0,  10);
  args.togetherplot_reqs.emplace_back("time_invratio_histogram", "", "_zoom5",  0,  5);
  args.togetherplot_reqs.emplace_back("time_invratio_histogram", "", "_zoom5",  0,  5);

  args.togetherplot_reqs.emplace_back("time_calc_gpu", "", "",  -1,  -1);


  args.togetherplot_reqs.emplace_back("time_histogram_modified", "", "",  -1,  -1);

  args.togetherplot_reqs.emplace_back("unmatched_E_hist", "", "", -1, -1, -1, -1, smallest_num_bins);
  args.togetherplot_reqs.emplace_back("unmatched_E_hist", "", "_zoom1", -50, 50);
  args.togetherplot_reqs.emplace_back("unmatched_E_hist", "", "_zoom2", -20, 20);
  args.togetherplot_reqs.emplace_back("unmatched_Et_hist", "", "", -1, -1);
  args.togetherplot_reqs.emplace_back("unmatched_Et_hist", "", "_zoom1", -50, 50);
  args.togetherplot_reqs.emplace_back("unmatched_Et_hist", "", "_zoom2", -20, 20);
  args.togetherplot_reqs.emplace_back("unmatched_sizes_hist", "", "", -1, -1);
  args.togetherplot_reqs.emplace_back("unmatched_sizes_hist", "", "_zoom", 0, 1000);
  args.togetherplot_reqs.emplace_back("unmatched_sizes_hist_cumul", "", "", -1, -1);
  args.togetherplot_reqs.emplace_back("unmatched_sizes_hist_cumul", "", "_zoom1", 0, 1000);
  args.togetherplot_reqs.emplace_back("unmatched_sizes_hist_cumul", "", "_zoom2", 0, 500);
  args.togetherplot_reqs.emplace_back("unmatched_sizes_hist_cumul", "", "_zoom3", 0, 250);
  args.togetherplot_reqs.emplace_back("unmatched_sizes_hist_cumul", "", "_zoom4", 0, 100);
  args.togetherplot_reqs.emplace_back("unmatched_eta_hist", "", "", -1, -1);
  args.togetherplot_reqs.emplace_back("unmatched_eta_hist", "", "_full", -7.5, 7.5);
  args.togetherplot_reqs.emplace_back("unmatched_eta_hist", "", "_zoom", -3, 3);
  args.togetherplot_reqs.emplace_back("unmatched_phi_hist", "", "", -1, -1);

  args.togetherplot_reqs.emplace_back("Et_hist", "", "", -1, -1, -1, -1, default_num_bins);
  args.togetherplot_reqs.emplace_back("Et_hist", "", "_zoom1", 0, 200);
  args.togetherplot_reqs.emplace_back("Et_hist", "", "_zoom2", 0, 100);
  args.togetherplot_reqs.emplace_back("E_hist", "", "", -1, -1);
  args.togetherplot_reqs.emplace_back("E_hist", "", "_zoom", -1500, 1500);
  args.togetherplot_reqs.emplace_back("cluster_size_hist", "", "", -1, -1);
  args.togetherplot_reqs.emplace_back("cluster_size_hist", "", "_zoom", 0, 1000);
  args.togetherplot_reqs.emplace_back("cluster_size_hist_cumul", "", "", -1, -1);
  args.togetherplot_reqs.emplace_back("cluster_size_hist_cumul", "", "_zoom1", 0, 1000);
  args.togetherplot_reqs.emplace_back("cluster_size_hist_cumul", "", "_zoom2", 0, 500);
  args.togetherplot_reqs.emplace_back("cluster_size_hist_cumul", "", "_zoom3", 0, 250);
  args.togetherplot_reqs.emplace_back("cluster_size_hist_cumul", "", "_zoom4", 0, 100);
  args.togetherplot_reqs.emplace_back("cluster_eta_hist", "", "", -1, -1);
  args.togetherplot_reqs.emplace_back("cluster_eta_hist", "", "_full", -7.5, 7.5);
  args.togetherplot_reqs.emplace_back("cluster_eta_hist", "", "_zoom0", -50, 50);
  args.togetherplot_reqs.emplace_back("cluster_eta_hist", "", "_zoom1", -10, 10);
  args.togetherplot_reqs.emplace_back("cluster_eta_hist", "", "_zoom2", -3, 3);
  args.togetherplot_reqs.emplace_back("cluster_phi_hist", "", "", -1, -1);
  args.togetherplot_reqs.emplace_back("cluster_phi_hist", "", "_full", -pi<double>, pi<double>);

  args.togetherplot_reqs.emplace_back("delta_R_hist", "", "", -1,  -1);
  args.togetherplot_reqs.emplace_back("delta_R_hist", "", "_zoom1", 0.,  0.25);
  args.togetherplot_reqs.emplace_back("delta_R_hist", "", "_zoom2", 0.,  0.001);
  args.togetherplot_reqs.emplace_back("delta_R_hist", "", "_zoom3", 0.,  0.0001);

  args.togetherplot_reqs.emplace_back("delta_R_hist_cumul", "", "", -1,  -1);
  args.togetherplot_reqs.emplace_back("delta_R_hist_cumul", "", "_zoom1", 0.,  0.25);
  args.togetherplot_reqs.emplace_back("delta_R_hist_cumul", "", "_zoom2", 0.,  0.001);
  args.togetherplot_reqs.emplace_back("delta_R_hist_cumul", "", "_zoom3", 0.,  0.0001);


  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster", "", "", -1, -1, -1, -1, small_num_bins);
  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster", "", "_zoom1", 0, 2500);
  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster", "", "_zoom1p5", 0, 1000);
  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster", "", "_zoom2", 0, 500);
  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster", "", "_zoom2p5", 0, 125);
  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster", "", "_zoom3", 0, 100);

  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster", "", "_zoom4", -0.5, 50.5, -1, -1, 51);

  args.togetherplot_reqs.emplace_back("rel_diff_cell_per_cluster", "", "", -1, -1, -1, -1, small_num_bins);
  args.togetherplot_reqs.emplace_back("rel_diff_cell_per_cluster", "", "_zoom1", 0, 0.5);
  args.togetherplot_reqs.emplace_back("rel_diff_cell_per_cluster", "", "_zoom2", 0., 0.25);
  args.togetherplot_reqs.emplace_back("rel_diff_cell_per_cluster", "", "_zoom3", 0., 0.05);
  args.togetherplot_reqs.emplace_back("rel_diff_cell_per_cluster", "", "_zoom4", 0., 0.01);

  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster_cumul", "", "", -1, -1);
  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster_cumul", "", "_zoom1", 0, 2500);
  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster_cumul", "", "_zoom1p5", 0, 1000);
  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster_cumul", "", "_zoom2", 0, 500);
  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster_cumul", "", "_zoom2p5", 0, 125);
  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster_cumul", "", "_zoom3", 0, 100);

  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster_cumul", "", "_zoom3p5", -0.5, 75.5, -1, -1, 76);

  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster_cumul", "", "_zoom4", -0.5, 50.5, -1, -1, 51);

  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster_cumul", "", "_zoom5", -0.5, 25.5, -1, -1, 26);

  args.togetherplot_reqs.emplace_back("diff_cell_per_cluster_cumul", "", "_zoom6", -0.5, 10.5, -1, -1, 11);

  args.togetherplot_reqs.emplace_back("rel_diff_cell_per_cluster_cumul", "", "", -1, -1, -1, -1, small_num_bins);
  args.togetherplot_reqs.emplace_back("rel_diff_cell_per_cluster_cumul", "", "_zoom1", 0, 0.5);
  args.togetherplot_reqs.emplace_back("rel_diff_cell_per_cluster_cumul", "", "_zoom2", 0., 0.25);
  args.togetherplot_reqs.emplace_back("rel_diff_cell_per_cluster_cumul", "", "_zoom2p5", 0., 0.2);
  args.togetherplot_reqs.emplace_back("rel_diff_cell_per_cluster_cumul", "", "_zoom3", 0., 0.05);
  args.togetherplot_reqs.emplace_back("rel_diff_cell_per_cluster_cumul", "", "_zoom4", 0., 0.01);
  args.togetherplot_reqs.emplace_back("rel_diff_cell_per_cluster_cumul", "", "_zoom5", 0., 0.005);
  args.togetherplot_reqs.emplace_back("rel_diff_cell_per_cluster_cumul", "", "_zoom6", 0., 0.002);

  args.togetherplot_reqs.emplace_back("equal_cells_delta_R", "", "", -1,  -1, -1, -1, default_num_bins);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_R", "", "_zoom1", 0.,  0.25);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_R", "", "_zoom2", 0.,  0.001);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_R", "", "_zoom3", 0.,  0.0001);

  args.togetherplot_reqs.emplace_back("equal_cells_delta_R_cumul", "", "", -1,  -1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_R_cumul", "", "_zoom1", 0.,  0.25);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_R_cumul", "", "_zoom2", 0.,  0.001);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_R_cumul", "", "_zoom3", 0.,  0.0001);


  args.togetherplot_reqs.emplace_back("equal_cells_delta_eta", "", "", -1,  -1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_eta", "", "_zoom1", -10,  10);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_eta", "", "_zoom2", -1,  1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_eta", "", "_zoom3", -0.25,  0.25);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_eta", "", "_zoom4", -0.1,  0.1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_eta", "", "_zoom5", -0.01,  0.01);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_eta", "", "_zoom6", -0.001,  0.001);


  args.togetherplot_reqs.emplace_back("equal_cells_delta_phi", "", "", -1,  -1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_phi", "", "_full", -pi<double>,  pi<double>);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_phi", "", "_zoom1", -1,  1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_phi", "", "_zoom2", -0.1,  0.1);

  args.togetherplot_reqs.emplace_back("equal_cells_delta_E", "", "", -1,  -1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom1", -1000,  1000);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom2", -500,  500);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom3", -100,  100);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom4", -50,  50);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom5", -10,  10);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom6", -1,  1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom7", -0.1,  0.1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom8", -0.01,  0.01);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E", "", "_zoom9", -0.001,  0.001);

  args.togetherplot_reqs.emplace_back("equal_cells_delta_E_rel", "", "", -1,  -1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E_rel", "", "_zoom1", -1,  1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E_rel", "", "_zoom2", -0.5,  0.5);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E_rel", "", "_zoom3", -0.1,  0.1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E_rel", "", "_zoom4", -0.01,  0.01);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E_rel", "", "_zoom5", -0.001,  0.001);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E_rel", "", "_zoom5p5", -0.0004,  0.0004);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_E_rel", "", "_zoom6", -0.0001,  0.0001);

  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et", "", "", -1,  -1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom1", -1000,  1000);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom2", -500,  500);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom3", -100,  100);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom4", -50,  50);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom5", -10,  10);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom6", -1,  1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom7", -0.1,  0.1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom8", -0.01,  0.01);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et", "", "_zoom9", -0.001,  0.001);

  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et_rel", "", "", -1,  -1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et_rel", "", "_zoom1", -1,  1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et_rel", "", "_zoom2", -0.5,  0.5);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et_rel", "", "_zoom3", -0.1,  0.1);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et_rel", "", "_zoom4", -0.01,  0.01);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et_rel", "", "_zoom5", -0.001,  0.001);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et_rel", "", "_zoom5p5", -0.0002,  0.0002);
  args.togetherplot_reqs.emplace_back("equal_cells_delta_Et_rel", "", "_zoom6", -0.0001,  0.0001);

  args.togetherplot_reqs.emplace_back("delta_eta_hist", "", "", -1,  -1);
  args.togetherplot_reqs.emplace_back("delta_eta_hist", "", "_zoom1", -10,  10);
  args.togetherplot_reqs.emplace_back("delta_eta_hist", "", "_zoom3", -1,  1);
  args.togetherplot_reqs.emplace_back("delta_eta_hist", "", "_zoom3", -0.25,  0.25);
  args.togetherplot_reqs.emplace_back("delta_eta_hist", "", "_zoom4", -0.1,  0.1);
  args.togetherplot_reqs.emplace_back("delta_eta_hist", "", "_zoom5", -0.01,  0.01);
  args.togetherplot_reqs.emplace_back("delta_eta_hist", "", "_zoom6", -0.001,  0.001);

  args.togetherplot_reqs.emplace_back("delta_phi_hist", "", "", -1,  -1);
  args.togetherplot_reqs.emplace_back("delta_phi_hist", "", "_full", -pi<double>,  pi<double>);

  args.togetherplot_reqs.emplace_back("delta_E_hist", "", "", -1,  -1);
  args.togetherplot_reqs.emplace_back("delta_E_hist", "", "_zoom1", -1000,  1000);
  args.togetherplot_reqs.emplace_back("delta_E_hist", "", "_zoom2", -500,  500);
  args.togetherplot_reqs.emplace_back("delta_E_hist", "", "_zoom2p5", -200,  200);
  args.togetherplot_reqs.emplace_back("delta_E_hist", "", "_zoom3", -100,  100);
  args.togetherplot_reqs.emplace_back("delta_E_hist", "", "_zoom4", -50,  50);
  args.togetherplot_reqs.emplace_back("delta_E_hist", "", "_zoom5", -10,  10);
  args.togetherplot_reqs.emplace_back("delta_E_hist", "", "_zoom6", -1,  1);
  args.togetherplot_reqs.emplace_back("delta_E_hist", "", "_zoom7", -0.1,  0.1);
  args.togetherplot_reqs.emplace_back("delta_E_hist", "", "_zoom8", -0.01,  0.01);
  args.togetherplot_reqs.emplace_back("delta_E_hist", "", "_zoom9", -0.001,  0.001);

  args.togetherplot_reqs.emplace_back("delta_Et_hist", "", "", -1,  -1);
  args.togetherplot_reqs.emplace_back("delta_Et_hist", "", "_zoom1", -1000,  1000);
  args.togetherplot_reqs.emplace_back("delta_Et_hist", "", "_zoom2", -500,  500);
  args.togetherplot_reqs.emplace_back("delta_Et_hist", "", "_zoom3", -100,  100);
  args.togetherplot_reqs.emplace_back("delta_Et_hist", "", "_zoom4", -50,  50);
  args.togetherplot_reqs.emplace_back("delta_Et_hist", "", "_zoom4p5", -20,  20);
  args.togetherplot_reqs.emplace_back("delta_Et_hist", "", "_zoom5", -10,  10);
  args.togetherplot_reqs.emplace_back("delta_Et_hist", "", "_zoom6", -1,  1);
  args.togetherplot_reqs.emplace_back("delta_Et_hist", "", "_zoom7", -0.1,  0.1);
  args.togetherplot_reqs.emplace_back("delta_Et_hist", "", "_zoom8", -0.01,  0.01);
  args.togetherplot_reqs.emplace_back("delta_Et_hist", "", "_zoom9", -0.001,  0.001);


  args.togetherplot_reqs.emplace_back("delta_E_rel_hist", "", "", -1,  -1);
  args.togetherplot_reqs.emplace_back("delta_E_rel_hist", "", "_zoom0", -1000,  1000);
  args.togetherplot_reqs.emplace_back("delta_E_rel_hist", "", "_zoom1", -100,  100);
  args.togetherplot_reqs.emplace_back("delta_E_rel_hist", "", "_zoom2", -10,  10);
  args.togetherplot_reqs.emplace_back("delta_E_rel_hist", "", "_zoom3", -1,  1);
  args.togetherplot_reqs.emplace_back("delta_E_rel_hist", "", "_zoom4", -0.5,  0.5);
  args.togetherplot_reqs.emplace_back("delta_E_rel_hist", "", "_zoom5", -0.1,  0.1);
  args.togetherplot_reqs.emplace_back("delta_E_rel_hist", "", "_zoom6", -0.01,  0.01);
  args.togetherplot_reqs.emplace_back("delta_E_rel_hist", "", "_zoom7", -0.001,  0.001);
  args.togetherplot_reqs.emplace_back("delta_E_rel_hist", "", "_zoom8", -0.0001,  0.0001);

  args.togetherplot_reqs.emplace_back("delta_Et_rel_hist", "", "", -1,  -1);
  args.togetherplot_reqs.emplace_back("delta_Et_rel_hist", "", "_zoom0", -1000,  1000);
  args.togetherplot_reqs.emplace_back("delta_Et_rel_hist", "", "_zoom1", -100,  100);
  args.togetherplot_reqs.emplace_back("delta_Et_rel_hist", "", "_zoom2", -10,  10);
  args.togetherplot_reqs.emplace_back("delta_Et_rel_hist", "", "_zoom3", -1,  1);
  args.togetherplot_reqs.emplace_back("delta_Et_rel_hist", "", "_zoom4", -0.5,  0.5);
  args.togetherplot_reqs.emplace_back("delta_Et_rel_hist", "", "_zoom5", -0.1,  0.1);
  args.togetherplot_reqs.emplace_back("delta_Et_rel_hist", "", "_zoom6", -0.01,  0.01);
  args.togetherplot_reqs.emplace_back("delta_Et_rel_hist", "", "_zoom7", -0.001,  0.001);
  args.togetherplot_reqs.emplace_back("delta_Et_rel_hist", "", "_zoom8", -0.0001,  0.0001);
}

bool setup_arguments(ProgArgs& args, int argc, char** argv)
{
  args.in_paths.clear();
  args.names.clear();
  args.out_path = "./plots/";
  args.min_similarity = default_min_similarity;
  args.term_weight = default_term_weight;
  args.grow_weight = default_grow_weight;
  args.seed_weight = default_seed_weight;
  args.place_titles = false;
  args.plot_label = "";
  args.normalize_hists = false;
  args.skip_almost_everything = false;
  args.use_ATLAS_style = true;

  args.plot_reqs.clear();
  args.togetherplot_reqs.clear();


  if (argc < 2)
    {
      std::cout << "Expected arguments: <list of input dirs ...>\n"
		   "Optional arguments: [-o <output dir>]\n"
                   "                    [-s <maximum similarity>]\n"
		   "                    [-w <term cell weight> <grow cell weight> <seed cell weight>]\n"
		   "                    [-n <list of plotter names>]\n"
                   "                    [-t {for plots with titles}]\n"
                   "                    [-l <plot labels>]\n"
	           "                    [-i <line-separated file with separate plots> (<line-separated file with together plots>)]\n"
	           "                    [-r {to renormalize histograms with the number of events}]\n"
	           "                    [-c {to skip filling anything other than cell information}]\n"
	           "                    [-b {to use the default style rather than ATLAS}]"
		<< std::endl;
      return true;
    }

  int arg = 1;

  bool specified_plot_files = false;

  for (; arg < argc; ++arg)
    {
      std::string text(argv[arg]);
      if (text == "-o" || text == "-s" || text == "-w" || text == "-n")
	{
	  break;
	}
      if (!std::filesystem::exists(text))
	{
	  std::cout<<"ERROR: Input directory '" << text << "' does not exist!"<< std::endl;
	  return true;
	}
      args.in_paths.push_back(text);
    }
  for (; arg < argc; ++arg)
    {
      if (!std::strcmp(argv[arg], "-o"))
	{
	  if (argc > arg + 1)
	    {
	      ++arg;
	      args.out_path = argv[arg];
	    }
	  else
	    {
	      std::cout << "ERROR: '-o' provided without specifying output dir!" << std::endl;
	      return true;
	    }
	}
      else if(!std::strcmp(argv[arg], "-s"))
	{
	  if (argc > arg + 1)
	    {
	      ++arg;
	      args.min_similarity = std::atof(argv[arg]);
	    }
	  else
	    {
	      std::cout << "ERROR: '-s' provided without specifying minimum similarity!" << std::endl;
	      return true;
	    }
	}
      else if(!std::strcmp(argv[arg], "-w"))
	{
	  if (argc > arg + 3)
	    {
	      args.term_weight = std::atof(argv[arg + 1]);
	      args.grow_weight = std::atof(argv[arg + 2]);
	      args.seed_weight = std::atof(argv[arg + 3]);
	      arg += 3;
	    }
	  else
	    {
	      std::cout << "ERROR: '-w' provided without properly specifying the weights!" << std::endl;
	      return true;
	    } 
	}
      else if(!std::strcmp(argv[arg], "-n"))
	{
	  if (argc > arg + args.in_paths.size())
	    {
	      ++arg;
	      for (int i = 0; i < args.in_paths.size() && arg < argc; ++i, ++arg)
		{
		  args.names.emplace_back(argv[arg]);
		}
	    }
	  else
	    {
	      std::cout << "ERROR: '-n' provided with an insufficient number of plots!" << std::endl;
	      return true;
	    } 
	}
      else if(!std::strcmp(argv[arg], "-t"))
	{
	  args.place_titles = true;
	}
      else if(!std::strcmp(argv[arg], "-l"))
	{
	  if (argc > arg + 1)
	    {
	      ++arg;
	      args.plot_label = argv[arg];
	    }
	  else
	    {
	      std::cout << "ERROR: '-l' provided without specifying plot label!" << std::endl;
	      return true;
	    }
	}
      else if(!std::strcmp(argv[arg], "-i"))
	{
	  if (argc > arg + 1)
	    {
	      ++arg;
	      read_plots_file(argv[arg], args.plot_reqs);
	      if (argc > arg + 1)
		{
		  ++arg;
		  read_plots_file(argv[arg], args.togetherplot_reqs);
		}
	      specified_plot_files = true;
	    }
	  else
	    {
	      std::cout << "ERROR: '-i' provided without specifying valid plot files!" << std::endl;
	      return true;
	    }
	}
      else if(!std::strcmp(argv[arg], "-r"))
	{
	  args.normalize_hists = true;
	}
      else if(!std::strcmp(argv[arg], "-c"))
	{
	  args.skip_almost_everything = true;
	}
      else if(!std::strcmp(argv[arg], "-b"))
	{
	  args.use_ATLAS_style = false;
	}
    }

  if (!std::filesystem::exists(args.out_path))
    {
      if (std::filesystem::create_directories(args.out_path))
        {
          std::cout << "WARNING: Output path '" << args.out_path << "' did not exist and was created!"<< std::endl;
        }
      else
        {
          std::cout << "ERROR: Output path '" << args.out_path << "' did not exist and couldn't be created!"<< std::endl;
          return true;
        }
    }

  ClusterPlotter::place_title = args.place_titles;
  ClusterPlotter::normalize = args.normalize_hists;
  ClusterPlotter::place_ATLAS_label = args.use_ATLAS_style;

  if(!specified_plot_files)
    {
      set_default_plots(args);
    }

  return false;
}




void do_plots (const ProgArgs& args, const std::string& plotter_name, const std::string& required_file_type)
{

  std::cout << "Plotting:\n";
  for (int i = 0; i < args.in_paths.size(); ++i)
    {
      std::string name = (args.names.size() > 0 ? args.names[i] : "");
      for (int k = name.size(); k < 32; ++k)
	{
	  name += " ";
	}
      std::cout << "          " << name << " " << args.in_paths[i] << "\n";
    }
  std::cout << std::endl;

  std::vector<ClusterPlotter> plotters;

  plotters.reserve(args.in_paths.size());

  TogetherPlot together;

  for (const auto& in_path : args.in_paths)
    {

      std::map<unsigned int, std::filesystem::path> reference_files, test_files;
  
      setup_files(in_path + "/default_validation", required_file_type, reference_files);
      setup_files(in_path + "/modified_validation", required_file_type, test_files);
      if (test_files.size() > 0)
	{
	  plotters.emplace_back(in_path + "/default_validation/geometryData.diag",
				reference_files,
				test_files,
				in_path + "/default_times.txt",
				in_path + "/modified_times.txt",
				args.min_similarity,
				args.term_weight,
				args.grow_weight,
				args.seed_weight,
				args.skip_almost_everything);

	  if (args.names.size() > 0 && args.names.size() >= plotters.size())
	    {
	      plotters.back().plotter_name = args.names[plotters.size() - 1];
	    }

	  if (!args.use_ATLAS_style)
	    {
	      plotters.back().Styles[0].line.color = 1;
	      plotters.back().Styles[1].line.color = 1;
	      plotters.back().Styles[2].line.color = 1;

	      plotters.back().Styles[0].fill.style = 3004;
	      plotters.back().Styles[1].fill.style = 3005;
	      plotters.back().Styles[2].fill.style = 1001;
	    }
	  

	  ClusterPlotter &plotter(plotters.back());


	  const std::string out_dir = args.out_path + "/plots_" + std::to_string(plotters.size());

	  
	  if (!std::filesystem::exists(out_dir))
	    {
	      if (std::filesystem::create_directories(out_dir))
		{
		  std::cout << "WARNING: Output path '" << out_dir << "' did not exist and was created!"<< std::endl;
		}
	      else
		{
		  std::cout << "ERROR: Output path '" << out_dir << "' did not exist and couldn't be created!"<< std::endl;
		  continue;
		}
	    }
	  
	  std::cout << "\n              Plotting " << plotter.plotter_name << " from '" << in_path << "' to '" << out_dir << "'\n" << std::endl;

	  plotter.plot_label = args.plot_label;

	  const int prev_num_bins = plotter.num_bins;

	  for (const auto & el : args.plot_reqs)
	    {
	      if (el.bin_size > 0)
		{
		  plotter.num_bins = el.bin_size;
		}
	      if (el.labelx > 0)
		{
		  ClusterPlotter::labelstart_x = el.labelx;
		}
	      if (el.labely > 0)
		{
		  ClusterPlotter::labelstart_y = el.labely;
		}
	      plotter.plot(el.name, el.xmin, el.xmax, el.ymin, el.ymax, out_dir, el.pref, el.suf);
	    }
	  plotter.num_bins = prev_num_bins;
	}
    }



  if (args.use_ATLAS_style)
    {
      plotters.front().Styles[0].line.color = kOrange + 3;
      plotters.front().Styles[0].line.style = 3;
      plotters.front().Styles[1].line.color = kRed;
      plotters.back().Styles[1].line.style = 1;
      plotters.front().Styles[2].line.color = kRed;
      plotters.back().Styles[1].line.style = 1;

      plotters.back().Styles[0].line.color = kCyan - 5;
      plotters.back().Styles[0].line.style = 4;
      plotters.back().Styles[1].line.color = kBlue;
      plotters.back().Styles[1].line.style = 2;
      plotters.back().Styles[2].line.color = kBlue;
      plotters.back().Styles[2].line.style = 2;
    }
  else
    {
      plotters.front().Styles[0].fill.color = kOrange + 3;
      plotters.front().Styles[1].fill.color = kRed;
      plotters.front().Styles[2].fill.color = kRed;
      
      plotters.back().Styles[0].fill.color = kCyan - 5;
      plotters.back().Styles[1].fill.color = kBlue;
      plotters.back().Styles[2].fill.color = kBlue;

      plotters.front().Styles[0].fill.style = 3007;
      plotters.front().Styles[1].fill.style = 3004;
      plotters.front().Styles[2].fill.style = 3004;

      plotters.back().Styles[0].fill.style = 3006;
      plotters.back().Styles[1].fill.style = 3005;
      plotters.back().Styles[2].fill.style = 3005;
    }
  for (const auto& plotter : plotters)
    {
      together.add_plot(&plotter);
    }

  std::cout << "\n              Plotting together.\n" << std::endl;
  
  {
    const int prev_num_bins_front = plotters.front().num_bins;
    const int prev_num_bins_back = plotters.back().num_bins;

    for (const auto & el : args.togetherplot_reqs)
      {
	if (el.bin_size > 0)
	  {
	    plotters.front().num_bins = el.bin_size;
	    plotters.back().num_bins = el.bin_size;
	  }
	together.plot(el.name, el.xmin, el.xmax, el.ymin, el.ymax, args.out_path, el.pref, el.suf);
      }
    
    plotters.front().num_bins = prev_num_bins_front;
    plotters.back().num_bins = prev_num_bins_back;

  }

  std::cout << "\n              Done!\n" << std::endl;

}


std::unique_ptr<TStyle> CreateAtlasStyle() 
{
  std::unique_ptr<TStyle> atlasStyle = std::make_unique<TStyle>("ATLAS","Atlas style");

  // use plain black on white colors
  Int_t icol=0; // WHITE
  atlasStyle->SetFrameBorderMode(icol);
  atlasStyle->SetFrameFillColor(icol);
  atlasStyle->SetCanvasBorderMode(icol);
  atlasStyle->SetCanvasColor(icol);
  atlasStyle->SetPadBorderMode(icol);
  atlasStyle->SetPadColor(icol);
  atlasStyle->SetStatColor(icol);
  //atlasStyle->SetFillColor(icol); // don't use: white fill color for *all* objects

  // set the paper & margin sizes
  atlasStyle->SetPaperSize(20,26);

  // set margin sizes
  atlasStyle->SetPadTopMargin(0.05);
  atlasStyle->SetPadRightMargin(0.05);
  atlasStyle->SetPadBottomMargin(0.16);
  atlasStyle->SetPadLeftMargin(0.16);

  // set title offsets (for axis label)
  atlasStyle->SetTitleXOffset(1.4);
  atlasStyle->SetTitleYOffset(1.4);

  // use large fonts
  //Int_t font=72; // Helvetica italics
  Int_t font=42; // Helvetica
  Double_t tsize=0.05;
  atlasStyle->SetTextFont(font);

  atlasStyle->SetTextSize(tsize);
  atlasStyle->SetLabelFont(font,"x");
  atlasStyle->SetTitleFont(font,"x");
  atlasStyle->SetLabelFont(font,"y");
  atlasStyle->SetTitleFont(font,"y");
  atlasStyle->SetLabelFont(font,"z");
  atlasStyle->SetTitleFont(font,"z");
  
  atlasStyle->SetLabelSize(tsize,"x");
  atlasStyle->SetTitleSize(tsize,"x");
  atlasStyle->SetLabelSize(tsize,"y");
  atlasStyle->SetTitleSize(tsize,"y");
  atlasStyle->SetLabelSize(tsize,"z");
  atlasStyle->SetTitleSize(tsize,"z");

  // use bold lines and markers
  atlasStyle->SetMarkerStyle(20);
  atlasStyle->SetMarkerSize(1.2);
  atlasStyle->SetHistLineWidth(2.);
  atlasStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  // get rid of X error bars (as recommended in ATLAS figure guidelines)
  atlasStyle->SetErrorX(0.0001);
  // get rid of error bar caps
  atlasStyle->SetEndErrorSize(0.);

  // do not display any of the standard histogram decorations
  atlasStyle->SetOptTitle(0);
  //atlasStyle->SetOptStat(1111);
  atlasStyle->SetOptStat(0);
  //atlasStyle->SetOptFit(1111);
  atlasStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  atlasStyle->SetPadTickX(1);
  atlasStyle->SetPadTickY(1);

  return std::move(atlasStyle);

}

int main(int argc, char **argv)
{

  TH1::AddDirectory(false);
  ProgArgs args;
    
  if (setup_arguments(args, argc, argv))
    {
      return 1;
    }

  std::unique_ptr<TStyle> atlasStyle = CreateAtlasStyle();
  
  if (args.use_ATLAS_style)
    {
      gROOT->SetStyle("ATLAS");
      gROOT->ForceStyle();
    }
 
  do_plots(args, "", "Maker");
  //"Maker"

  //do_plots(args, "ClusterSplitter_", "Splitter");
  //"Splitter
  //do_plots(args, "", "");
  //Neither "Splitter" nor "Maker"
 
}
