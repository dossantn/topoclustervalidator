COMPILER = gcc

ROOTFLAGS = $(shell root-config --cflags)

ROOTLIBS = $(shell root-config --glibs)

OLEVEL = -O3

EXTRAFLAGS = -std=c++17 -march=native -fPIC -lm -lstdc++ -lstdc++fs

FULLFOLDER = ../Results/diffnoise

all: clusterplotter

clusterplotter: clusterplotter.cpp ClusterPlotter.h GeneralDefines.h
	$(COMPILER) clusterplotter.cpp -o clusterplotter $(OLEVEL) $(EXTRAFLAGS) $(ROOTFLAGS) $(ROOTLIBS)

clean:
	rm -f clusterplotter
	rm -f *~

ClusterPlotter.h: GeneralDefines.h ClusterPlotterPlots.h
	touch $@

GeneralDefines.h ClusterPlotterPlots.h:
	touch $@

plotsdiff: clusterplotter
	./clusterplotter ../Results/diffnoise/run_ttbar/ ../Results/diffnoise/run_jets/ -o diffnoise -n "t#bar{t} MC (<#mu> = 80)" "Di-jet MC (<#mu> = 20)" -t 0 -l "Simulation Preliminary" -i ATLAS_plots.txt ATLAS_plots.txt -r

plotssame: clusterplotter
	./clusterplotter ../Results/samenoise/run_ttbar/ ../Results/samenoise/run_jets/ -o samenoise -n "t#bar{t} MC (<#mu> = 80)" "Di-jet MC (<#mu> = 20)" -t 0 -l "Simulation Preliminary" -i ATLAS_plots.txt ATLAS_plots.txt -r

plots: plotsdiff plotssame

plotsfull: clusterplotter
	./clusterplotter $(FULLFOLDER)/run_ttbar/ $(FULLFOLDER)/run_jets/ -o plotsfull -n "t#bar{t} MC (<#mu> = 80)" "Di-jet MC (<#mu> = 20)" -t 0 -l "Simulation Preliminary" -b -r
