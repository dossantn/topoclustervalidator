#ifndef CLUSTERPLOTTER_H
#define CLUSTERPLOTTER_H

#include <vector>
#include <string>
#include <set>
#include <limits>
#include <utility>

#include <functional>

#include <sstream>
#include <numeric>
#include <iomanip>

#include "GeneralDefines.h"


#include <TROOT.h>	  // the TROOT object is the entry point to the ROOT system
#include <TH1F.h>
#include <TCanvas.h>  // graphics canvas
#include <TFile.h>
#include <TStyle.h>
#include <TPaveStats.h>
#include <TH2F.h>
#include <THStack.h>

#include <TLatex.h>
#include <TLine.h>
#include <TLegend.h>
#include <TPave.h>
#include <TPad.h>
#include <TMarker.h>



template <class Obj, class F, class ... Args>
void operate_on_regions (Obj * arr, const float eta, F && f, Args && ... args)
//f must take an Obj * that points to the object in which to operate.
{
  f(arr, std::forward<Args>(args)...);
  const float abs_eta = std::abs(eta);
  if (abs_eta < regCuts[0])
    {
      f(arr + 1, std::forward<Args>(args)...);
    }
  else if (abs_eta < regCuts[1])
    {
      f(arr + 2, std::forward<Args>(args)...);
    }
  else if (abs_eta < regCuts[2])
    {
      f(arr + 3, std::forward<Args>(args)...);
    }
  else
    {
      //std::cout << "ERROR: " << abs_eta << " is outside expected eta parameters!" << std::endl;
    }
}

template <class Obj, class F, class ... Args>
void operate_on_types (Obj * arr, const float SNR, F && f, Args && ... args)
//f must take an Obj * that points to the object in which to operate.
{
  f(arr, std::forward<Args>(args)...);
  if (SNR > SNR_thresholds[2])
    {
      f(arr + 1, std::forward<Args>(args)...);
    }
  else if (SNR > SNR_thresholds[1])
    {
      f(arr + 2, std::forward<Args>(args)...);
    }
  if (SNR > SNR_thresholds[0])
    {
      f(arr + 3, std::forward<Args>(args)...);
    }
}

struct ClusterData;

/*!
  Put a variable to its most positive possible value.
*/
template <class T> void set_to_highest(T && val)
{
  if constexpr (std::is_arithmetic_v< std::decay_t<T> >)
    {
      val = std::numeric_limits< std::decay_t<T> >::max();
    }
  else if constexpr (std::is_same_v< std::decay_t<T>, ClusterData >)
    {
      val.set_all_to_highest();
    }
  else
    {
      //Bleep.
    }
}

/*!
  Put a variable to its most negative possible value.
*/
template <class T> void set_to_lowest(T && val)
{
  if constexpr (std::is_arithmetic_v< std::decay_t<T> >)
    {
      val = std::numeric_limits< std::decay_t<T> >::lowest();
    }
  else if constexpr (std::is_same_v< std::decay_t<T>, ClusterData >)
    {
      val.set_all_to_lowest();
    }
  else
    {
      //Bleep.
    }
}

struct ClusterData
{
  int num_cells;
  float energy;
  float transverse_energy;
  float eta;
  float phi;
  double abs_energy;
  double eta_post;
  double phi_post;
  double energy_post;

  ClusterData(const int n_c = 0, const float en = 0, const float e_t = 0, const float h = 0, const float f = 0, const double abs_e = 0,
	      const double h_post = 0, const double f_post = 0, const double en_post = 0):
    num_cells(n_c), energy(en), transverse_energy(e_t), eta(h), phi(f), abs_energy(abs_e),
    eta_post(h_post), phi_post(f_post), energy_post(en_post)
  {
  }

  void set_all_to_highest()
  {
    set_to_highest(num_cells);
    set_to_highest(energy);
    set_to_highest(transverse_energy);
    set_to_highest(eta);
    set_to_highest(phi);
    set_to_highest(abs_energy);
    set_to_highest(eta_post);
    set_to_highest(phi_post);
    set_to_highest(energy_post);
  }

  void set_all_to_lowest()
  {
    set_to_lowest(num_cells);
    set_to_lowest(energy);
    set_to_lowest(transverse_energy);
    set_to_lowest(eta);
    set_to_lowest(phi);
    set_to_lowest(abs_energy);
    set_to_lowest(eta_post);
    set_to_lowest(phi_post);
    set_to_lowest(energy_post);
  }

  void set(const int n_num, const float n_ene, const float n_et, const float n_eta, const float n_phi, const double abs_e)
  {
    num_cells = n_num;
    energy = n_ene;
    transverse_energy = n_et;
    eta = n_eta;
    phi = n_phi;
    abs_energy = abs_e;
  }

  void set(const int n_num, const float n_ene, const float n_et, const float n_eta, const float n_phi)
  {
    num_cells = n_num;
    energy = n_ene;
    transverse_energy = n_et;
    eta = n_eta;
    phi = n_phi;
  }

  void set_to_min(const int n_num, const float n_ene, const float n_et,
		  const float n_eta, const float n_phi, const double n_abs_e,
		  const double n_eta_post, const double n_phi_post, const double n_ene_post)
  {
    num_cells = std::min(num_cells, n_num);
    energy = std::min(energy, n_ene);
    transverse_energy = std::min(transverse_energy, n_et);
    eta = std::min(eta, n_eta);
    phi = std::min(phi, n_phi);
    abs_energy = std::min(abs_energy, n_abs_e);
    eta_post = std::min(eta_post, n_eta_post);
    phi_post = std::min(phi_post, n_phi_post);
    energy_post = std::min(energy_post, n_ene_post);
  }


  void set_to_max(const int n_num, const float n_ene, const float n_et,
		  const float n_eta, const float n_phi, const double n_abs_e,
		  const double n_eta_post, const double n_phi_post, const double n_ene_post)
  {
    num_cells = std::max(num_cells, n_num);
    energy = std::max(energy, n_ene);
    transverse_energy = std::max(transverse_energy, n_et);
    eta = std::max(eta, n_eta);
    phi = std::max(phi, n_phi);
    abs_energy = std::max(abs_energy, n_abs_e);
    eta_post = std::max(eta_post, n_eta_post);
    phi_post = std::max(phi_post, n_phi_post);
    energy_post = std::max(energy_post, n_ene_post);
  }

  void set_to_min(const int n_num, const float n_ene, const float n_et, const float n_eta, const float n_phi, const double n_abs_e)
  {
    num_cells = std::min(num_cells, n_num);
    energy = std::min(energy, n_ene);
    transverse_energy = std::min(transverse_energy, n_et);
    eta = std::min(eta, n_eta);
    phi = std::min(phi, n_phi);
    abs_energy = std::min(abs_energy, n_abs_e);
  }

  void set_to_max(const int n_num, const float n_ene, const float n_et, const float n_eta, const float n_phi, const double n_abs_e)
  {
    num_cells = std::max(num_cells, n_num);
    energy = std::max(energy, n_ene);
    transverse_energy = std::max(transverse_energy, n_et);
    eta = std::max(eta, n_eta);
    phi = std::max(phi, n_phi);
    abs_energy = std::max(abs_energy, n_abs_e);
  }

  void set_to_min(const ClusterData& other)
  {
    set_to_min(other.num_cells, other.energy, other.transverse_energy, other.eta,
	       other.phi, other.abs_energy, other.eta_post, other.phi_post, other.energy_post);
  }

  void set_to_max(const ClusterData& other)
  {
    set_to_max(other.num_cells, other.energy, other.transverse_energy, other.eta,
	       other.phi, other.abs_energy, other.eta_post, other.phi_post, other.energy_post);
  }

  double delta_R(const double other_eta, const double other_phi) const
  {
    const double delta_eta = (eta - other_eta);
    const double delta_phi = minDiffPhi(phi, other_phi);
    
    return std::sqrt(delta_eta * delta_eta + delta_phi * delta_phi);
  }

  double delta_R(const ClusterData& other) const
  {
    return delta_R(other.eta, other.phi);
  }

  double delta_R_post(const double other_eta, const double other_phi) const
  {
    const double delta_eta = (eta_post - other_eta);
    const double delta_phi = minDiffPhi(phi_post, other_phi);
    
    return std::sqrt(delta_eta * delta_eta + delta_phi * delta_phi);
  }

  double delta_R_post(const ClusterData& other) const
  {
    return delta_R_post(other.eta_post, other.phi_post);
  }

  double delta_R_post() const
  {
    return delta_R(eta_post, phi_post);
  }

};

constexpr double default_min_similarity = 0.9;
constexpr double default_term_weight = 1.0;
constexpr double default_grow_weight = 250;
constexpr double default_seed_weight = 5000;


struct EventData
{
  std::vector<ClusterData> ref_clusters, test_clusters;
  std::vector<float> SNR_array;
  std::vector<float> energy_array;
  std::vector<int> ref_tag_array, test_tag_array;
  std::vector<int> r2t_matches, t2r_matches;
  std::vector<int> ref_unmatched, test_unmatched;

  std::vector<double> delta_R_array;
  std::vector<double> delta_Et_rel_array;
  //Indexed by reference cluster!
  //(We seem to get less reference clusters, on average.)

  ClusterData min_vals[4], max_vals[4], unmatched_min_vals[4], unmatched_max_vals[4];

  float min_reg_energy[4], max_reg_energy[4], min_reg_SNR[4], max_reg_SNR[4];
  //By region (all, central, end-cap or forward)

  float min_type_energy[4], max_type_energy[4], min_type_SNR[4], max_type_SNR[4];
  //By type (all, seed, grow, terminal)

  double min_delta_R[4], max_delta_R[4];

  double min_delta_E[4], max_delta_E[4];

  double min_delta_Et[4], max_delta_Et[4];

  double min_delta_E_rel[4], max_delta_E_rel[4];

  double min_delta_Et_rel[4], max_delta_Et_rel[4];

  double min_delta_eta[4], max_delta_eta[4];

  double min_delta_phi[4], max_delta_phi[4];

  int max_cluster_number[4], min_cluster_number[4];
  int max_unmatched_number[4], min_unmatched_number[4];

  bool test_has_more[4];

  int cluster_diff[4];

  double cell_diff_frac_reg[4], cell_diff_frac_type[4];

  std::vector<bool> cluster_same_cells[4];

  void find_matches(const double min_similarity = default_min_similarity,
		    const double term_weight = default_term_weight,
		    const double grow_weight = default_grow_weight,
		    const double seed_weight = default_seed_weight)
  { 
    std::vector<int> similarity_map(test_clusters.size() * ref_clusters.size(), 0.f);

    std::vector<double> ref_normalization(ref_clusters.size(), 0.f);
    std::vector<double> test_normalization(test_clusters.size(), 0.f);
    

    for (int i = 0; i < NCALOCELLS; ++i)
      {
	const int ref_tag = ref_tag_array[i];
	const int test_tag = test_tag_array[i];
	const double SNR = std::abs(SNR_array[i]);
	const double weight = SNR * ( SNR > SNR_thresholds[2] ? seed_weight :
				      (
				       SNR > SNR_thresholds[1] ? grow_weight :
				       (
					SNR > SNR_thresholds[0] ? term_weight :
					0 
					)
				       )
				      );
	if (ref_tag >= 0 && test_tag >= 0)
	  {
	    similarity_map[test_tag * ref_clusters.size() + ref_tag] += weight;
	  }
	if (ref_tag >= 0)
	  {
	    ref_normalization[ref_tag] += weight;
	  }
	if (test_tag >= 0)
	  {
	    test_normalization[test_tag] += weight;
	  }
      }

    /*
      for (int tc = 0; tc < test_clusters.size(); ++tc)
      {
      for (int rc = 0; rc < ref_clusters.size(); ++rc)
      {
      const double simil = similarity_map[tc * ref_clusters.size() + rc];
      if (simil > 0)
      {
      std::cout << tc << " " << rc << " " << simil << " " << simil/ref_normalization[rc] << " " << simil/test_normalization[tc] << std::endl;
      }
      }
      }

      std::cout << " --- " << std::endl;
    */

    
    //In essence, the Gale-Shapley Algorithm

    std::vector<std::vector<int>> sorted_GPU_matches;

    sorted_GPU_matches.reserve(test_clusters.size());

    for (int testc = 0; testc < test_clusters.size(); ++testc)
      {
	std::vector<int> sorter(ref_clusters.size());
	std::iota(sorter.begin(), sorter.end(), 0);

	std::sort(sorter.begin(), sorter.end(), 
		  [&](const int a, const int b) { 
		    const double a_weight = similarity_map[testc * ref_clusters.size() + a];
		    const double b_weight = similarity_map[testc * ref_clusters.size() + b];
		    return a_weight > b_weight;
		  }
		  );
	
	int wanted_size = 0;

	for (; wanted_size < sorter.size(); ++wanted_size)
	  {
	    const double match_weight = similarity_map[testc * ref_clusters.size() + sorter[wanted_size]] / test_normalization [testc];
	    if (match_weight < min_similarity)
	      {
		break;
	      }
	  }

	//Yeah, we could do a binary search for best worst-case complexity,
	//but we are expecting 1~2 similar clusters and the rest garbage,
	//so we're expecting only 1~2 iterations.
	//This actually means all that sorting is way way overkill,
	//but we must make sure in the most general case that this works...

	sorter.resize(wanted_size);

	sorted_GPU_matches.push_back(sorter);
      }

    int num_iter = 0;

    constexpr int max_iter = 32;

    std::vector<double> matched_weights(ref_clusters.size(), -1.);

    std::vector<int> skipped_matching(test_clusters.size(), 0);

    for (int stop_counter = 0; stop_counter < test_clusters.size() && num_iter < max_iter; ++num_iter)
      {
	stop_counter = 0;
	for (int testc = 0; testc < sorted_GPU_matches.size(); ++testc)
	  {
	    if(skipped_matching[testc] < sorted_GPU_matches[testc].size())
	      {
		const int match_c = sorted_GPU_matches[testc][skipped_matching[testc]];
		const double match_weight = similarity_map[testc * ref_clusters.size() + match_c] / ref_normalization[match_c];
		if (match_weight >= min_similarity && match_weight > matched_weights[match_c])
		  {
		    const int prev_match = r2t_matches[match_c];
		    if (prev_match >= 0)
		      {
			++skipped_matching[prev_match];
			--stop_counter;
		      }
		    r2t_matches[match_c] = testc;
		    matched_weights[match_c] = match_weight;
		    ++stop_counter;
		  }
		else
		  {
		    ++skipped_matching[testc];
		  }
	      }
	    else
	      {
		++stop_counter;
	      }
	  }
      }

    ref_unmatched.clear();
    test_unmatched.clear();

    for (int i = 0; i < r2t_matches.size(); ++i)
      {
	const int match = r2t_matches[i];
	if (r2t_matches[i] < 0)
	  {
	    ref_unmatched.push_back(i);
	  }
	else
	  {
	    t2r_matches[match] = i;
	  }
      }

    for (int i = 0; i < t2r_matches.size(); ++i)
      {
	if (t2r_matches[i] < 0)
	  {
	    test_unmatched.push_back(i);
	  }
      }

    std::cout << "Matched in " << num_iter << " iterations: "
	      << r2t_matches.size() - ref_unmatched.size() << " / " << r2t_matches.size() << "  ||  " 
	      << t2r_matches.size() - test_unmatched.size() << " / " << t2r_matches.size() << "  ||  " << ref_unmatched.size() << " | " << test_unmatched.size() << std::endl;
    /*
      {
      std::vector<int> testc(t2r_matches.size(), 0);

      for (int i = 0; i < r2t_matches.size(); ++i)
      {
      const int res = r2t_matches[i];
      if (res >= 0)
      {
      testc[res] += 1;
      }
      }

      for (int i = 0; i < testc.size(); ++i)
      {
      if (testc[i] != 1)
      {
      std::cout << i << ": " << testc[i] << "\n";
      }
      }

      std::cout << "\n\n----------" << std::endl;
      }
    */

  }


  EventData(const FullCaloGeometry_t &geometry,
	    const debugClusters_t & reference,
	    const debugClusters_t & test,
	    const double min_similarity = default_min_similarity, 
	    const double term_weight = default_term_weight,
	    const double grow_weight = default_grow_weight,
	    const double seed_weight = default_seed_weight,
	    const bool skip_almost_everything = false):
    ref_clusters(reference.nClusters),
    test_clusters(test.nClusters),
    SNR_array(NCALOCELLS),
    energy_array(NCALOCELLS),
    ref_tag_array(NCALOCELLS),
    test_tag_array(NCALOCELLS),
    r2t_matches(reference.nClusters, -1),
    t2r_matches(test.nClusters, -1),
    delta_R_array(reference.nClusters, -1),
    delta_Et_rel_array(reference.nClusters, -2)
    //delta_Et_rel >= -1 by definition
  {
    std::vector<int> ref_seed_cells(ref_clusters.size(), -1);
    std::vector<int> test_seed_cells(test_clusters.size(), -1);
    std::vector<float> ref_sc_snr(ref_clusters.size(), 0);
    std::vector<float> test_sc_snr(test_clusters.size(), 0);

    for (int i = 0; i < 4; ++i)
      {
	set_to_highest(min_reg_energy[i]);
	set_to_highest(min_reg_SNR[i]);

	set_to_lowest(max_reg_energy[i]);
	set_to_lowest(max_reg_SNR[i]);


	set_to_highest(min_type_energy[i]);
	set_to_highest(min_type_SNR[i]);

	set_to_lowest(max_type_energy[i]);
	set_to_lowest(max_type_SNR[i]);

      }
    for (int i = 0; i < NCALOCELLS; ++i)
      {
	const float this_ref_energy = reference.cellE[i];
	const float this_test_energy = test.cellE[i];
	const float this_energy = (this_ref_energy + this_test_energy)/2.f;


	const float this_noise = geometry.noise[i];

	float this_SNR;

	if (this_noise > 0.0f)
	  {
	    this_SNR = std::abs(this_energy / this_noise);
	  }
	else
	  {
	    this_SNR = 1e-6;
	    //What should we do in this case?
	  }

	SNR_array[i] = this_SNR;

	const float this_scale_energy = this_energy * 1e-3;

	energy_array[i] = this_scale_energy;

	operate_on_regions(min_reg_energy, geometry.eta[i], [&](float * en){ *en = std::min(*en, this_scale_energy); });
	operate_on_regions(max_reg_energy, geometry.eta[i], [&](float * en){ *en = std::max(*en, this_scale_energy); });

	operate_on_regions(min_reg_SNR, geometry.eta[i], [&](float * snr){ *snr = std::min(*snr, this_SNR); });
	operate_on_regions(max_reg_SNR, geometry.eta[i], [&](float * snr){ *snr = std::max(*snr, this_SNR); });


	operate_on_types(min_type_energy, this_SNR, [&](float * en){ *en = std::min(*en, this_scale_energy); });
	operate_on_types(max_type_energy, this_SNR, [&](float * en){ *en = std::max(*en, this_scale_energy); });

	operate_on_types(min_type_SNR, geometry.eta[i], [&](float * snr){ *snr = std::min(*snr, this_SNR); });
	operate_on_types(max_type_SNR, geometry.eta[i], [&](float * snr){ *snr = std::max(*snr, this_SNR); });

	const int ref_tag = reference.seedTag[i];
	const int test_tag = test.seedTag[i];

	ref_tag_array[i] = ref_tag;
	test_tag_array[i] = test_tag;

	if (ref_tag >= 0)
	  {
	    if (this_SNR > ref_sc_snr[ref_tag])
	      {
		ref_sc_snr[ref_tag] = this_SNR;
		ref_seed_cells[ref_tag] = i;
	      }
	  }
	if (test_tag >= 0)
	  {
	    if (this_SNR > test_sc_snr[test_tag])
	      {
		test_sc_snr[test_tag] = this_SNR;
		test_seed_cells[test_tag] = i;
	      }
	  }
	
      }
    

    for (int i = 0; i < ref_clusters.size(); ++i)
      {
	ref_clusters[i].abs_energy = 0;
      }

    for (int i = 0; i < test_clusters.size(); ++i)
      {
	test_clusters[i].abs_energy = 0;
      }

    for (int i = 0; i < NCALOCELLS; ++i)
      {

	const int ref_tag = ref_tag_array[i];
	const int test_tag = test_tag_array[i];

	const float this_energy = energy_array[i];

	const float this_abs_energy = std::abs(this_energy);

	if (ref_tag >= 0 && ref_seed_cells[ref_tag] >= 0)
	  {
	    ref_clusters[ref_tag].abs_energy += this_abs_energy;
	    ref_clusters[ref_tag].eta_post += geometry.eta[i] * this_abs_energy;
	    ref_clusters[ref_tag].phi_post += proxim_ath(geometry.phi[i], geometry.phi[ref_seed_cells[ref_tag]]) * this_abs_energy;
	    ref_clusters[ref_tag].energy_post += this_energy;
	  }
	if (test_tag >= 0 && test_seed_cells[test_tag] >= 0)
	  {
	    test_clusters[test_tag].abs_energy += this_abs_energy;
	    test_clusters[test_tag].eta_post += geometry.eta[i] * this_abs_energy;
	    test_clusters[test_tag].phi_post += proxim_ath(geometry.phi[i], geometry.phi[test_seed_cells[test_tag]]) * this_abs_energy;
	    test_clusters[test_tag].energy_post += this_energy;
	  }
      }

    if(!skip_almost_everything)
      {
	int ref_cluster_counter[4], test_cluster_counter[4];

	for (int i = 0; i < 4; ++i)
	  {
	    set_to_lowest(max_vals[i]);

	    set_to_highest(min_vals[i]);

	    ref_cluster_counter[i] = 0;
	    test_cluster_counter[i] = 0;
	  }

	for (int i = 0; i < reference.nClusters; ++i)
	  {

	    const int num_cells = reference.clusterNCells[i];
	    const float energy = reference.clusterEnergy[i] * 1e-3;
	    const float transverse_energy = reference.clusterEt[i] * 1e-3;
	    //So the energies are in GeV!
	    const float eta = reference.clusterEta[i];
	    const float phi = reference.clusterPhi[i];

	    ref_clusters[i].set(num_cells, energy, transverse_energy, eta, phi);

	    operate_on_regions(max_vals, eta, [&](ClusterData * clu){ clu->set_to_max(ref_clusters[i]); });
	    operate_on_regions(min_vals, eta, [&](ClusterData * clu){ clu->set_to_min(ref_clusters[i]); });

	    operate_on_regions(ref_cluster_counter, eta, [](int *val){ *val = *val + 1; } );

	    ref_clusters[i].eta_post /= ref_clusters[i].abs_energy;
	    ref_clusters[i].phi_post /= ref_clusters[i].abs_energy;
	    ref_clusters[i].phi_post = wrapPhi(ref_clusters[i].phi_post);

	    ref_clusters[i].abs_energy *= 1e-3;
	    ref_clusters[i].energy_post *= 1e-3;
	  }

	for (int i = 0; i < test.nClusters; ++i)
	  {
	    const int num_cells = test.clusterNCells[i];
	    const float energy = test.clusterEnergy[i] * 1e-3;
	    const float transverse_energy = test.clusterEt[i] * 1e-3;
	    //So the energies are in GeV!
	    const float eta = test.clusterEta[i];
	    const float phi = test.clusterPhi[i];

	    test_clusters[i].set(num_cells, energy, transverse_energy, eta, phi);

	    operate_on_regions(max_vals, eta, [&](ClusterData * clu){ clu->set_to_max(test_clusters[i]); });
	    operate_on_regions(min_vals, eta, [&](ClusterData * clu){ clu->set_to_min(test_clusters[i]); });

	    operate_on_regions(test_cluster_counter, eta, [](int *val){ *val = *val + 1; } );

	    test_clusters[i].eta_post /= test_clusters[i].abs_energy;
	    test_clusters[i].phi_post /= test_clusters[i].abs_energy;
	    test_clusters[i].phi_post = wrapPhi(test_clusters[i].phi_post);

	    test_clusters[i].abs_energy *= 1e-3;
	    test_clusters[i].energy_post *= 1e-3;
	  }

	for (int i = 0; i < 4; ++i)
	  {
	    max_cluster_number[i] = std::max(ref_cluster_counter[i], test_cluster_counter[i]);
	    min_cluster_number[i] = std::min(ref_cluster_counter[i], test_cluster_counter[i]);
	    test_has_more[i] = test_cluster_counter[i] > ref_cluster_counter[i];
	    cluster_diff[i] = int(test_cluster_counter[i]) - int(ref_cluster_counter[i]);
	  }

	find_matches(min_similarity, term_weight, grow_weight, seed_weight);

	for (int i = 0; i < 4; ++i)
	  {
	    set_to_lowest(max_delta_R[i]);
	    set_to_highest(min_delta_R[i]);

	    set_to_lowest(max_delta_E[i]);
	    set_to_highest(min_delta_E[i]);

	    set_to_lowest(max_delta_E_rel[i]);
	    set_to_highest(min_delta_E_rel[i]);

	    set_to_lowest(max_delta_Et[i]);
	    set_to_highest(min_delta_Et[i]);

	    set_to_lowest(max_delta_Et_rel[i]);
	    set_to_highest(min_delta_Et_rel[i]);

	    set_to_lowest(max_delta_eta[i]);
	    set_to_highest(min_delta_eta[i]);

	    set_to_lowest(max_delta_phi[i]);
	    set_to_highest(min_delta_phi[i]);	  

	    ref_cluster_counter[i] = 0;
	    test_cluster_counter[i] = 0;

	    set_to_lowest(unmatched_max_vals[i]);

	    set_to_highest(unmatched_min_vals[i]);
	  }

	for (int refc = 0; refc < r2t_matches.size(); ++refc)
	  {
	    const int this_match = r2t_matches[refc];
	    if (this_match >= 0)
	      {
		const ClusterData * this_cluster = &(ref_clusters[refc]);
		const ClusterData * match_cluster = &(test_clusters[this_match]);

		const double delta_R = match_cluster->delta_R(*this_cluster);
	    
		const double delta_E = match_cluster->energy - this_cluster->energy;

		const double delta_E_rel = std::abs(delta_E/this_cluster->energy);

		const double delta_Et = match_cluster->transverse_energy - this_cluster->transverse_energy;

		const double delta_Et_rel = delta_Et/this_cluster->transverse_energy;


		const double delta_eta = (match_cluster->eta - this_cluster->eta);
		const double delta_phi = minDiffPhi(match_cluster->phi, this_cluster->phi);

		delta_R_array[refc] = delta_R;
	    
		delta_Et_rel_array[refc] = delta_Et_rel;

		operate_on_regions(max_delta_R, ref_clusters[refc].eta, [&](double * dr){ *dr = std::max(*dr, delta_R); });
		operate_on_regions(min_delta_R, ref_clusters[refc].eta, [&](double * dr){ *dr = std::min(*dr, delta_R); });

		operate_on_regions(max_delta_E, ref_clusters[refc].eta, [&](double * dE){ *dE = std::max(*dE, delta_E); });
		operate_on_regions(min_delta_E, ref_clusters[refc].eta, [&](double * dE){ *dE = std::min(*dE, delta_E); });

		operate_on_regions(max_delta_Et, ref_clusters[refc].eta, [&](double * dE){ *dE = std::max(*dE, delta_Et); });
		operate_on_regions(min_delta_Et, ref_clusters[refc].eta, [&](double * dE){ *dE = std::min(*dE, delta_Et); });

		operate_on_regions(max_delta_E_rel, ref_clusters[refc].eta, [&](double * dE){ *dE = std::max(*dE, delta_E_rel); });
		operate_on_regions(min_delta_E_rel, ref_clusters[refc].eta, [&](double * dE){ *dE = std::min(*dE, delta_E_rel); });

		operate_on_regions(max_delta_Et_rel, ref_clusters[refc].eta, [&](double * dE){ *dE = std::max(*dE, delta_Et_rel); });
		operate_on_regions(min_delta_Et_rel, ref_clusters[refc].eta, [&](double * dE){ *dE = std::min(*dE, delta_Et_rel); });

		operate_on_regions(max_delta_phi, ref_clusters[refc].eta, [&](double * dphi){ *dphi = std::max(*dphi, delta_phi); });
		operate_on_regions(min_delta_phi, ref_clusters[refc].eta, [&](double * dphi){ *dphi = std::min(*dphi, delta_phi); });

		operate_on_regions(max_delta_eta, ref_clusters[refc].eta, [&](double * deta){ *deta = std::max(*deta, delta_eta); });
		operate_on_regions(min_delta_eta, ref_clusters[refc].eta, [&](double * deta){ *deta = std::min(*deta, delta_eta); });
	      }
	    else
	      {
		operate_on_regions(ref_cluster_counter, ref_clusters[refc].eta, [](int *val){ *val = *val + 1; } );
	    
		operate_on_regions(unmatched_max_vals, ref_clusters[refc].eta, [&](ClusterData * clu){ clu->set_to_max(test_clusters[refc]); });
		operate_on_regions(unmatched_min_vals, ref_clusters[refc].eta, [&](ClusterData * clu){ clu->set_to_min(test_clusters[refc]); });
	      }
	    //Unmatched clusters get DeltaR == -1
	  }

	for (const auto test_nomatch : test_unmatched)
	  {
	    operate_on_regions(test_cluster_counter, test_clusters[test_nomatch].eta, [](int *val){ *val = *val + 1; } );
	    operate_on_regions(unmatched_max_vals, test_clusters[test_nomatch].eta,
			       [&](ClusterData * clu){ clu->set_to_max(test_clusters[test_nomatch]); });
	    operate_on_regions(unmatched_min_vals, test_clusters[test_nomatch].eta,
			       [&](ClusterData * clu){ clu->set_to_min(test_clusters[test_nomatch]); });
	  }

	for (int i = 0; i < 4; ++i)
	  {
	    max_unmatched_number[i] = std::max(ref_cluster_counter[i], test_cluster_counter[i]);
	    min_unmatched_number[i] = std::min(ref_cluster_counter[i], test_cluster_counter[i]);
	  }


	int cell_diff_type[4];
	int cell_diff_reg[4];
	int cell_count_type[4];
	int cell_count_reg[4];

	for (int i = 0; i < 4; ++i)
	  {
	    cell_diff_type[i] = 0;
	    cell_diff_reg[i] = 0;
	    cluster_same_cells[i].resize(ref_clusters.size(), true);
	  }

	for (int i = 0; i < NCALOCELLS; ++i)
	  {
	    const int ref_tag = reference.seedTag[i];
	    const int test_tag = test.seedTag[i];
	    if (ref_tag < 0 && test_tag < 0)
	      {
		//Do nothing in this case: they are both off.
	      }
	    else if ( (ref_tag < 0 && test_tag >= 0) || (ref_tag >= 0 && test_tag < 0) ||
		      r2t_matches[ref_tag] != test_tag || t2r_matches[test_tag] != ref_tag )
	      {
		operate_on_types(cell_diff_type, SNR_array[i], [](int * count){ (*count) = *count + 1; });
		operate_on_regions(cell_diff_reg, geometry.eta[i], [](int * count){ (*count) = *count + 1; });
	      }

	    if (ref_tag >= 0 && r2t_matches[ref_tag] != test_tag)
	      {
		operate_on_types(cluster_same_cells, SNR_array[i], [&](std::vector<bool> *v){ (*v)[ref_tag] = false; });
	      }
	    if (test_tag >= 0 && t2r_matches[test_tag] != ref_tag && t2r_matches[test_tag] >= 0)
	      {
		operate_on_types(cluster_same_cells, SNR_array[i], [&](std::vector<bool> *v){ (*v)[t2r_matches[test_tag]] = false; });
	      }
	
	    operate_on_types(cell_count_type, SNR_array[i], [](int * count){ (*count) = *count + 1; });
	    operate_on_regions(cell_count_reg, geometry.eta[i], [](int * count){ (*count) = *count + 1; });
	  }

	for (int i = 0; i < 4; ++i)
	  {
	    cell_diff_frac_type[i] = double(cell_diff_type[i])/double(cell_count_type[i]);
	    cell_diff_frac_reg[i] = double(cell_diff_reg[i])/double(cell_count_reg[i]);
	  }
      }
  }


};

struct ClusterPlotter
{
  std::vector<EventData> events;
  std::unique_ptr<FullCaloGeometry_t> geometry;
  
  double min_energy_cut = 1e-3;

  int min_size_cut = 128;


  ClusterData min_vals[4], max_vals[4], unmatched_min_vals[4], unmatched_max_vals[4];

  double min_delta_R[4], max_delta_R[4];


  double min_delta_E[4], max_delta_E[4];

  double min_delta_Et[4], max_delta_Et[4];

  double min_delta_E_rel[4], max_delta_E_rel[4];

  double min_delta_Et_rel[4], max_delta_Et_rel[4];

  double min_delta_eta[4], max_delta_eta[4];

  double min_delta_phi[4], max_delta_phi[4];


  float min_reg_energy[4], max_reg_energy[4], min_reg_SNR[4], max_reg_SNR[4];
  //By region (all, central, end-cap or forward)

  float min_type_energy[4], max_type_energy[4], min_type_SNR[4], max_type_SNR[4];
  //By type (all, seed, grow, terminal)

  int max_cluster_number[4], min_cluster_number[4];

  int max_unmatched_number[4], min_unmatched_number[4];

  int max_cluster_diff[4], min_cluster_diff[4];

  ClusterPlotter(const std::filesystem::path & geometry_file,
		 const std::map<unsigned int, std::filesystem::path> & reference_files,
		 const std::map<unsigned int, std::filesystem::path> & test_files,
		 const std::filesystem::path & reference_times,
		 const std::filesystem::path & modified_times,
		 const double min_similarity = default_min_similarity,
		 const double term_weight = default_term_weight,
		 const double grow_weight = default_grow_weight,
		 const double seed_weight = default_seed_weight,
		 const bool skip_almost_everything = false):
  geometry(new FullCaloGeometry_t())
  {
    std::unique_ptr<debugClusters_t> reference_clusters(new debugClusters_t), test_clusters(new debugClusters_t);

    if (load_from_file(geometry_file, *geometry))
      {
	std::cerr << "ERROR: Unable to load geometry. Exiting..." << std::endl;
	exit(1);
      }

    
    for (auto it = test_files.begin(); it != test_files.end(); ++it)
      {
	const auto reference = reference_files.find(it->first);
	//Try to find the event among the reference files
	if (reference == reference_files.end())
	  //Didn't find the corresponding event
	  {
	    std::cout << "WARNING: Couldn't find event #" << it->first << " in reference! Skipping it." << std::endl;
	    continue;
	  }
	else
	  {
	    if (load_from_file(it->second, *test_clusters) || load_from_file(reference->second, *reference_clusters))
	      {
		std::cout << "Skipping event #" << it->first << std::endl;
	      }
	    else
	      { 
		events.emplace_back(*geometry, *reference_clusters, *test_clusters, min_similarity, term_weight, grow_weight, seed_weight, skip_almost_everything);
	      }
	  }
      }

    for (int i = 0; i < 4; ++i)
      {
	set_to_lowest(max_vals[i]);

	set_to_highest(min_vals[i]);

	set_to_highest(min_reg_energy[i]);
	set_to_highest(min_reg_SNR[i]);

	set_to_lowest(max_reg_energy[i]);
	set_to_lowest(max_reg_SNR[i]);

	set_to_highest(min_type_energy[i]);
	set_to_highest(min_type_SNR[i]);

	set_to_lowest(max_type_energy[i]);
	set_to_lowest(max_type_SNR[i]);

	set_to_lowest(max_delta_R[i]);
	set_to_highest(min_delta_R[i]);

	set_to_lowest(max_cluster_number[i]);
	set_to_highest(min_cluster_number[i]);

	set_to_lowest(max_unmatched_number[i]);
	set_to_highest(min_unmatched_number[i]);

	set_to_lowest(max_cluster_diff[i]);
	set_to_highest(min_cluster_diff[i]);

	
	set_to_lowest(max_delta_eta[i]);
	set_to_highest(min_delta_eta[i]);

	set_to_lowest(max_delta_phi[i]);
	set_to_highest(min_delta_phi[i]);
	  
	set_to_lowest(unmatched_max_vals[i]);

	set_to_highest(unmatched_min_vals[i]);
      }

    for (const auto & ev : events)
      {
	for (int i = 0; i < 4; ++i)
	  {
	    min_vals[i].set_to_min(ev.min_vals[i]);
	    max_vals[i].set_to_max(ev.max_vals[i]);

	    min_type_energy[i] = std::min(min_type_energy[i], ev.min_type_energy[i]);
	    min_type_SNR[i] = std::min(min_type_SNR[i], ev.min_type_SNR[i]);
	    min_reg_energy[i] = std::min(min_reg_energy[i], ev.min_reg_energy[i]);
	    min_reg_SNR[i] = std::min(min_reg_SNR[i], ev.min_reg_SNR[i]);

	    max_type_energy[i] = std::max(max_type_energy[i], ev.max_type_energy[i]);
	    max_type_SNR[i] = std::max(max_type_SNR[i], ev.max_type_SNR[i]);
	    max_reg_energy[i] = std::max(max_reg_energy[i], ev.max_reg_energy[i]);
	    max_reg_SNR[i] = std::max(max_reg_SNR[i], ev.max_reg_SNR[i]);
	    
	    min_delta_R[i] = std::min(min_delta_R[i], ev.min_delta_R[i]);
	    max_delta_R[i] = std::max(max_delta_R[i], ev.max_delta_R[i]);

	    min_delta_E[i] = std::min(min_delta_E[i], ev.min_delta_E[i]);
	    max_delta_E[i] = std::max(max_delta_E[i], ev.max_delta_E[i]);

	    min_delta_Et[i] = std::min(min_delta_Et[i], ev.min_delta_Et[i]);
	    max_delta_Et[i] = std::max(max_delta_Et[i], ev.max_delta_Et[i]);

	    min_delta_E_rel[i] = std::min(min_delta_E_rel[i], ev.min_delta_E_rel[i]);
	    max_delta_E_rel[i] = std::max(max_delta_E_rel[i], ev.max_delta_E_rel[i]);

	    min_delta_Et_rel[i] = std::min(min_delta_Et_rel[i], ev.min_delta_Et_rel[i]);
	    max_delta_Et_rel[i] = std::max(max_delta_Et_rel[i], ev.max_delta_Et_rel[i]);

	    min_cluster_number[i] = std::min( min_cluster_number[i], ev.max_cluster_number[i] );
	    max_cluster_number[i] = std::max( max_cluster_number[i], ev.min_cluster_number[i] );

	    min_unmatched_number[i] = std::min( min_unmatched_number[i], ev.max_unmatched_number[i] );
	    max_unmatched_number[i] = std::max( max_unmatched_number[i], ev.min_unmatched_number[i] );

	    min_cluster_diff[i] = std::min( min_cluster_diff[i], ev.cluster_diff[i] );
	    max_cluster_diff[i] = std::max( max_cluster_diff[i], ev.cluster_diff[i] );

	    min_delta_eta[i] = std::min(min_delta_eta[i], ev.min_delta_eta[i]);
	    max_delta_eta[i] = std::max(max_delta_eta[i], ev.max_delta_eta[i]);

	    min_delta_phi[i] = std::min(min_delta_phi[i], ev.min_delta_phi[i]);
	    max_delta_phi[i] = std::max(max_delta_phi[i], ev.max_delta_phi[i]);

	    unmatched_min_vals[i].set_to_min(ev.unmatched_min_vals[i]);
	    unmatched_max_vals[i].set_to_max(ev.unmatched_max_vals[i]);
	  }
      }

    populate_plots();

    load_times(reference_times, modified_times);
  }

  
  int num_bins = 250;

  int canvas_x = 800, canvas_y = 600;

  std::string plotter_name{"Results"};
  std::string ref_name{"CPU"}, test_name{"GPU"};
  std::string label_type_1{"Seed"}, label_type_2{"Growing"}, label_type_3{"Terminal"};
  std::string suffix_type_1{"seed"}, suffix_type_2{"grow"}, suffix_type_3{"term"};
  std::string label_region_1{"Center"}, label_region_2{"End-Cap"}, label_region_3{"Forward"};
  std::string suffix_region_1{"center"}, suffix_region_2{"endcap"}, suffix_region_3{"forward"};
  std::vector<std::string> file_extensions = {std::string("pdf"), std::string("eps"), std::string("png")};
  std::vector<std::string> print_options = {std::string("EmbedFonts"), std::string(), std::string()};
  //This is only valid for PDF files!
  //If file_extension is changed, this should be too!

  enum class StyleKinds
  {
    ref = 0, test, joined, Number
      };

  template <class T> struct PlotKinds
  {
    T ref;
    T test;
    T joined;
  };

  struct ElementStyle_t
  {
    Color_t color;
    Style_t style;
    float alpha;
  };
  
  struct GraphStyle_t
  {
    ElementStyle_t line, fill;
  };
  
  std::array<GraphStyle_t, int(StyleKinds::Number)>
    Styles{ GraphStyle_t{ ElementStyle_t{kBlue, 1, 1.0f}, ElementStyle_t{kBlue,  0/*3004*/, 1.0f} },   //Ref
            GraphStyle_t{ ElementStyle_t{kRed, 1, 1.0f}, ElementStyle_t{kRed,  0/*3005*/, 1.0f} },   //Test
	    GraphStyle_t{ ElementStyle_t{1, 1, 1.0f}, ElementStyle_t{kGray + 1, 0, 1.0f} } }; //Joined
  // Line,         Fill
  // (c, s, a)      (c, s, a)
  //So Styles[<kind>].<element>.<style>
  //(e. g. Styles[StyleKinds::test].fill.color)
  
  inline static bool place_title = false;
  inline static bool yaxis_bins = true;
  inline static std::string plot_label = "";
  inline static bool place_ATLAS_label = true;

  static std::string stringify_pretty_number (const double num)
  {
    std::stringstream sstr;
    sstr << std::setprecision(2) << std::fixed << num;
    //std::cout << sstr.str() << std::endl;
    return sstr.str();
  }

  static std::string approximate_string(const double val)
  {
    if (val >= 1e-2 && val <= 1e4)
      {
	return stringify_pretty_number(std::ceil(val * 1e3)*1e-3);
      }
    else
      {
	double log = std::floor(std::log10(val));
	return stringify_pretty_number(std::ceil(val * pow(10, 3 - log))*1e-3) + " #times 10^{" + std::to_string(int(log)) + "}";
      }
  }

  template <class Plot>
  static std::string get_bin_label(Plot * gr)
  {
    const double width = gr->GetXaxis()->GetBinWidth(1);
    std::string_view xtext = gr->GetXaxis()->GetTitle();
    const size_t pos_l = xtext.find_last_of('[');
    const size_t pos_r = xtext.find_last_of(']');
    if (pos_l == std::string_view::npos || pos_r == std::string_view::npos || pos_r < pos_l)
      {
	return approximate_string(width);
      }
    else
      {
	return approximate_string(width) + " " + std::string(xtext.substr(pos_l+1, pos_r - pos_l -1));
      }
  }

  static void ATLASLabel(Double_t x, Double_t y) 
  {
    TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
    l.SetNDC();
    l.SetTextFont(72);
    l.SetTextColor(1);

    double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());

    l.DrawLatex(x,y,"ATLAS");
    if (plot_label != "")
      {
	TLatex p; 
	p.SetNDC();
	p.SetTextFont(42);
	p.SetTextColor(1);
	p.DrawLatex(x+delx,y, plot_label.c_str());
	//    p.DrawLatex(x,y,"#sqrt{s}=900GeV");
      }
  }

  inline static double labelstart_x = 0.625;
  inline static double labelstart_y = 0.625;
  //Well, legend, but...
  inline static double labelsize_x = 0.25;
  inline static double labelsize_y = 0.25;

  inline static double labeloffset_x = -0.225;
  inline static double labeloffset_y = labelsize_y + 0.01;
  //These control the position of the "ATLAS ..." label
  //in relation to the plot legend.

  inline static double extraspacefactor_add = 0.;
  inline static double extraspacefactor_multiply = 2.5;

  inline static bool normalize = false;

  inline static std::string extra_text = "#sqrt{s} = 13 TeV";

  inline static double extralabeloffset_x = 0.075;
  inline static double extralabeloffset_y = -0.04;

  static void extra_text_label(Double_t x, Double_t y) 
  {
    TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
    l.SetNDC();
    l.SetTextColor(1);
    l.SetTextSize(0.04);

    l.DrawLatex(x,y,extra_text.c_str());
  }

  static void plot_one(TCanvas * cv, TH1 *gr, const std::string & file,
		       const std::vector<std::string> &exts,  const std::vector<std::string> & options)
  {
    cv->cd();
    cv->Clear();
    if (place_ATLAS_label)
      {
	gStyle->SetOptStat(0000);
	gStyle->SetOptTitle(place_title);
      
	if (yaxis_bins)
	  {
	    std::string title = gr->GetYaxis()->GetTitle();
	    title += " / " + get_bin_label(gr);
	    gr->GetYaxis()->SetTitle(title.c_str());
	  }
	gr->SetMaximum(gr->GetMaximum() * extraspacefactor_multiply + extraspacefactor_add);
	gr->Draw("");
	ATLASLabel(labelstart_x + labeloffset_x, labelstart_y + labeloffset_y);
	extra_text_label(labelstart_x + extralabeloffset_x, labelstart_y + extralabeloffset_y);
      }
    else
      {
	gStyle->SetOptStat(2200);
	gStyle->SetOptTitle(place_title);
	gr->Draw("");
      }
    cv->SetLogy(1);
    for (size_t i = 0; i < exts.size(); ++i)
      {
	cv->SaveAs((file + "." + exts[i]).c_str(), options[i].c_str());
      }
  }

  static void plot_one(TCanvas * cv, TH2 *gr, const std::string & file,
		       const std::vector<std::string> &exts,  const std::vector<std::string> & options)
  {
    cv->cd();
    cv->Clear();
    if (place_ATLAS_label)
      {
	gStyle->SetOptStat(0000);
      }
    else
      {
	gStyle->SetOptStat(2200);
      }
    gStyle->SetOptTitle(place_title);
    gr->Draw("colsz");
    cv->SetLogy(0);
    cv->SetLogz(1);
    for (size_t i = 0; i < exts.size(); ++i)
      {
	cv->SaveAs((file + "." + exts[i]).c_str(), options[i].c_str());
      }
  }

  static void plot_one(TCanvas *cv, THStack *hs, const std::string & file,
		       const std::vector<std::string> &exts,  const std::vector<std::string> & options)
  {
    cv->cd();
    cv->Clear();
    if (place_ATLAS_label)
      {
	gStyle->SetOptStat(0000);
      }
    else
      {
	gStyle->SetOptStat(2200);
      }
    gStyle->SetOptTitle(place_title);
    std::vector <Style_t> styles(hs->GetNhists());
    TIter iter(hs->GetHists());
    iter.Begin();
    for (int i = 0; iter != iter.End(); iter(), ++i)
      {
	TH1F * hist = (TH1F *) *iter;
	styles[i] = hist->GetLineStyle();
	hist->SetLineStyle(0);
	if (place_ATLAS_label)
	  {
	    if (yaxis_bins)
	      {
		std::string title = hist->GetYaxis()->GetTitle();
		title += " / " + get_bin_label(hist);
		hist->GetYaxis()->SetTitle(title.c_str());
	      }
	    hist->SetMaximum(hist->GetMaximum() * extraspacefactor_multiply + extraspacefactor_add);
	  }
      }
    hs->Draw("nostack");
    if (place_ATLAS_label)
      {
	if (yaxis_bins)
	  {
	    std::string title = hs->GetYaxis()->GetTitle();
	    title += " / " + get_bin_label(hs);
	    hs->GetYaxis()->SetTitle(title.c_str());
	  }
	TLegend *leg = gPad->BuildLegend(labelstart_x,labelstart_y,labelstart_x+labelsize_x,labelstart_y+labelsize_y,"","l");
	if (place_ATLAS_label)
	  {
	    ATLASLabel(labelstart_x + labeloffset_x, labelstart_y + labeloffset_y);
	    extra_text_label(labelstart_x + extralabeloffset_x, labelstart_y + extralabeloffset_y);
	  }

	leg->SetBorderSize(0);
	leg->SetFillColor(0);
	leg->SetTextFont(42);
	leg->SetTextSize(0.0275);
      }
    else
      {
	gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
      }
    iter.Begin();
    for (int i = 0; iter != iter.End(); iter(), ++i)
      {
	TH1F * hist = (TH1F *) *iter;
	hist->SetLineStyle(styles[i]);
	styles[i] = hist->GetFillStyle();
	hist->SetFillStyle(0);
      }
    hs->Draw("nostack same");
    iter.Begin();
    for (int i = 0; iter != iter.End(); iter(), ++i)
      {
	TH1F * hist = (TH1F *) *iter;
	hist->SetFillStyle(styles[i]);
      }
    /* 
      //This is an attempt for plot overlap to work
      //without transparency/patterns.
      //It kinda does, but it isn't pretty...
    std::vector<std::unique_ptr<TH1F>> temps;
    TIter iter(hs->GetHists());
    for (iter.Begin(); iter != iter.End(); iter() )
      {
	temps.emplace_back(std::make_unique<TH1F>(* ((TH1F *) *iter)));
	temps.back()->SetLineColorAlpha(0, 0.);
	//temps.back()->Rebin(, "", );
	for(int i = 1; i <= temps.back()->GetNbinsX(); ++i)
	  {
	    double min = temps.back()->GetBinContent(i);
	    TIter other = iter;
	    for (other.Begin(); other != iter; other())
	      {
		TH1F * otherhistptr = (TH1F *) *other;
		min = std::min(min, otherhistptr->GetBinContent(i));
	      }
	    temps.back()->SetBinContent(i, min);
	    //We could just set to 0,
	    //but I'll keep like this
	    //so we can check what happens
	    //if we drop SetLineColorAlpha(0,0.)
	  }
	temps.back()->Draw("same");
	}
    */
    gPad->Modified();
    gPad->Update();
    cv->Update();
    cv->SetLogy(1);
    for (size_t i = 0; i < exts.size(); ++i)
      {
	cv->SaveAs((file + "." + exts[i]).c_str(), options[i].c_str());
      }
  }
  
  struct hist_stacker
  {
    std::unique_ptr<THStack> global, t1, t2, t3;
    hist_stacker(const std::string & histname, const std::string & hist_title, const std::string & x_label,
		 const std::string & y_label, const std::string & t1_label, const std::string & t2_label,
		 const std::string & t3_label):
      global(new THStack((histname + "_g").c_str(), (hist_title + " ;" + x_label + " ;" + y_label).c_str())),
      t1(new THStack((histname + "_t1").c_str(), (hist_title + " (" + t1_label + ") ;" + x_label + " ;" + y_label).c_str())),
      t2(new THStack((histname + "_t2").c_str(), (hist_title + " (" + t2_label + ") ;" + x_label + " ;" + y_label).c_str())),
      t3(new THStack((histname + "_t3").c_str(), (hist_title + " (" + t3_label + ") ;" + x_label + " ;" + y_label).c_str()))
    {
    }
  };
  
  template <class T>
  struct hist_group
  {
    std::unique_ptr<T> global, t1, t2, t3;
        
    void set_style(const GraphStyle_t & gs)
    {
      global->SetLineColorAlpha(gs.line.color,gs.line.alpha);
      global->SetLineStyle(gs.line.style);
      global->SetFillColorAlpha(gs.fill.color, gs.fill.alpha);
      global->SetFillStyle(gs.fill.style);
      
      t1->SetLineColorAlpha(gs.line.color,gs.line.alpha);
      t1->SetLineStyle(gs.line.style);
      t1->SetFillColorAlpha(gs.fill.color, gs.fill.alpha);
      t1->SetFillStyle(gs.fill.style);
      
      t2->SetLineColorAlpha(gs.line.color,gs.line.alpha);
      t2->SetLineStyle(gs.line.style);
      t2->SetFillColorAlpha(gs.fill.color, gs.fill.alpha);
      t2->SetFillStyle(gs.fill.style);
      
      t3->SetLineColorAlpha(gs.line.color,gs.line.alpha);
      t3->SetLineStyle(gs.line.style);
      t3->SetFillColorAlpha(gs.fill.color, gs.fill.alpha);
      t3->SetFillStyle(gs.fill.style);
    }
  };
  
  struct hist_group_1D : public hist_group<TH1F>
  {
    hist_group_1D(const std::array<double, 4> &min, const std::array<double, 4> &max, const int num_bins,
		  const std::string & histname, const std::string & hist_title, const std::string & x_label,
		  const std::string & y_label, const std::string & t1_label, const std::string & t2_label,
		  const std::string & t3_label)
    {
      global.reset(new TH1F( (histname + "_g").c_str(), (hist_title + " ;" + x_label + " ;" + y_label).c_str(),
			     num_bins, min[0], max[0]));
      t1.reset(new TH1F( (histname + "_t1").c_str(), (hist_title + " (" + t1_label + ") ;" + x_label + " ;" + y_label).c_str(),
			 num_bins, min[1], max[1]));
      t2.reset(new TH1F( (histname + "_t2").c_str(), (hist_title + " (" + t2_label + ") ;" + x_label + " ;" + y_label).c_str(),
			 num_bins, min[2], max[2]));
      t3.reset(new TH1F( (histname + "_t3").c_str(), (hist_title + " (" + t3_label + ") ;" + x_label + " ;" + y_label).c_str(),
			 num_bins, min[3], max[3]));
      /*
      std::cout << "Building " << histname << "\nRanges: ";
      for (int i = 0; i < 4; ++i)
	{
	  std::cout << "(" << min[i] << " | " << max[i] << ")";
	}
      std::cout << std::endl;
      */
    }
        
    hist_group_1D(const std::array<double, 4> &min, const std::array<double, 4> &max, const int num_bins,
		  const std::string & histname, const std::string & hist_title)
    {
      global.reset(new TH1F( (histname + "_g").c_str(), hist_title.c_str(), num_bins, min[0], max[0]));
      t1.reset(new TH1F( (histname + "_t1").c_str(), hist_title.c_str(), num_bins, min[1], max[1]));
      t2.reset(new TH1F( (histname + "_t2").c_str(), hist_title.c_str(), num_bins, min[2], max[2]));
      t3.reset(new TH1F( (histname + "_t3").c_str(), hist_title.c_str(), num_bins, min[3], max[3]));
      /*
      std::cout << "Building " << histname << "\nRanges: ";
      for (int i = 0; i < 4; ++i)
	{
	  std::cout << "(" << min[i] << " | " << max[i] << ")";
	}
      std::cout << std::endl;
      */
    }

    template <class F>
      void populate (F && f, const int cumulative, const size_t num_events)
    {
      f(this);
      if (cumulative != 0)
	{
	  global->Sumw2(false);
	  t1->Sumw2(false);
	  t2->Sumw2(false);
	  t3->Sumw2(false);
	  std::unique_ptr<TH1F> a((TH1F *) global->GetCumulative(cumulative > 0));
	  std::unique_ptr<TH1F> b((TH1F *) t1->GetCumulative(cumulative > 0));
	  std::unique_ptr<TH1F> c((TH1F *) t2->GetCumulative(cumulative > 0));
	  std::unique_ptr<TH1F> d((TH1F *) t3->GetCumulative(cumulative > 0));
	  a->Scale(1./global->GetEntries());
	  b->Scale(1./t1->GetEntries());
	  c->Scale(1./t2->GetEntries());
	  d->Scale(1./t3->GetEntries());
	  a.swap(global);
	  b.swap(t1);
	  c.swap(t2);
	  d.swap(t3);
	  global->Sumw2(false);
	  t1->Sumw2(false);
	  t2->Sumw2(false);
	  t3->Sumw2(false);
	}
      else if(normalize)
	{
	  global->Scale(1./num_events);
	  t1->Scale(1./num_events);
	  t2->Scale(1./num_events);
	  t3->Scale(1./num_events);
	  global->Sumw2(false);
	  t1->Sumw2(false);
	  t2->Sumw2(false);
	  t3->Sumw2(false);
	}
    }


    void add_to_stack(hist_stacker & stack)
    {
      stack.global->Add(global.get());
      stack.t1->Add(t1.get());
      stack.t2->Add(t2.get());
      stack.t3->Add(t3.get());
    }
  };
  
  struct hist_group_2D : public hist_group<TH2F>
  {
    hist_group_2D(const std::array<double, 4> &min_x, const std::array<double, 4> &max_x,
		  const std::array<double, 4> &min_y, const std::array<double, 4> &max_y, const int num_bins,
		  const std::string & histname, const std::string & hist_title, const std::string & x_label,
		  const std::string & y_label, const std::string & t1_label, const std::string & t2_label,
		  const std::string & t3_label)
    {
      global.reset(new TH2F( (histname + "_g").c_str(), (hist_title + " ;" + x_label + " ;" + y_label).c_str(),
			     num_bins, min_x[0], max_x[0], num_bins, min_y[0], max_y[0]));
      t1.reset(new TH2F( (histname + "_t1").c_str(), (hist_title + " (" + t1_label + ") ;" + x_label + " ;" + y_label).c_str(),
			 num_bins, min_x[1], max_x[1], num_bins, min_y[1], max_y[1]));
      t2.reset(new TH2F( (histname + "_t2").c_str(), (hist_title + " (" + t2_label + ") ;" + x_label + " ;" + y_label).c_str(),
			 num_bins, min_x[2], max_x[2], num_bins, min_y[2], max_y[2]));
      t3.reset(new TH2F( (histname + "_t3").c_str(), (hist_title + " (" + t3_label + ") ;" + x_label + " ;" + y_label).c_str(),
			 num_bins, min_x[2], max_x[3], num_bins, min_y[3], max_y[3]));
    }

    template <class F>
    void populate (F && f, const int ignore, const size_t also_ignore)
    {
      f(this);
    }
        
  };
  
  enum class PlotterKind
  {
    type = 0, region, time, type2D, region2D, Number
      };

  struct plotter_base
  {
    std::string hist_name, hist_title, x_label, y_label;
    const ClusterPlotter *parent;
  
    virtual PlotterKind plotter_kind() const = 0;
      
    virtual void plot(const std::string &path, const std::string& prefix = "", const std::string& suffix = "") const = 0;
    virtual void plot(const std::array<double, 4> &wanted_min_x, const std::array<double, 4> &wanted_max_x,
                      const std::string &path, const std::string& prefix = "", const std::string& suffix = "") const = 0;
    virtual void plot(const std::array<double, 4> &wanted_min_x, const std::array<double, 4> &wanted_max_x,
                      const std::array<double, 4> &wanted_min_y, const std::array<double, 4> &wanted_max_y,
                      const std::string &path, const std::string& prefix = "", const std::string& suffix = "") const = 0;
                      
    virtual void plot(const double wanted_min_x, const double wanted_max_x,
                      const std::string &path, const std::string& prefix = "", const std::string& suffix = "") const
    {
      this->plot(std::array<double, 4>{wanted_min_x, wanted_min_x, wanted_min_x, wanted_min_x},
                 std::array<double, 4>{wanted_max_x, wanted_max_x, wanted_max_x, wanted_max_x},
                 path, prefix, suffix);
    }      
    
    virtual void plot(const double wanted_min_x, const double wanted_max_x,
                      const double wanted_min_y, const double wanted_max_y,
                      const std::string &path, const std::string& prefix = "", const std::string& suffix = "") const
    {
      this->plot(std::array<double, 4>{wanted_min_x, wanted_min_x, wanted_min_x, wanted_min_x},
                 std::array<double, 4>{wanted_max_x, wanted_max_x, wanted_max_x, wanted_max_x},
                 std::array<double, 4>{wanted_min_y, wanted_min_y, wanted_min_y, wanted_min_y},
                 std::array<double, 4>{wanted_max_y, wanted_max_y, wanted_max_y, wanted_max_y},
                 path, prefix, suffix);
    }
  };
  
  template <class group_T>
  struct basic_plotter : public plotter_base
  {
  
    bool can_be_negative;

    int cumulative;
    //0 means no, < 0 means backward, > 0 means forward accumulation

    StyleKinds style_ref;
    std::function<void(group_T *)> populator;
        
    virtual void save(const std::string & base_name, const group_T & group) const = 0;
        
    virtual group_T create_group(const std::string& title_override = "") const
    {
      group_T ret = this->construct_group(title_override);
      ret.populate(populator, cumulative, parent->events.size());
      ret.set_style(parent->Styles[int(style_ref)]);
      return ret;
    }
    
    virtual group_T create_group(const std::array<double, 4> &wanted_min_x,
				 const std::array<double, 4> &wanted_max_x,
				 const std::array<double, 4> &wanted_min_y,
				 const std::array<double, 4> &wanted_max_y,
				 const std::string& title_override = "",
				 const bool force_range = false) const
    {
      group_T ret = this->construct_group(wanted_min_x, wanted_max_x, wanted_min_y, wanted_max_y, title_override, force_range);
      ret.populate(populator, cumulative, parent->events.size());
      ret.set_style(parent->Styles[int(style_ref)]);
      return ret;
    }
                                        
    virtual group_T create_group(const std::array<double, 4> &wanted_min_x,
				 const std::array<double, 4> &wanted_max_x,
				 const std::string& title_override = "",
				 const bool force_range = false) const
    {
      group_T ret = this->construct_group(wanted_min_x, wanted_max_x, title_override, force_range);
      ret.populate(populator, cumulative, parent->events.size());
      ret.set_style(parent->Styles[int(style_ref)]);
      return ret;
    }
    
    
    virtual group_T create_group(const double wanted_min_x, const double wanted_max_x,
				 const double wanted_min_y, const double wanted_max_y,
				 const std::string& title_override = "",
				 const bool force_range = false) const
    {
      group_T ret = this->construct_group(wanted_min_x, wanted_max_x, wanted_min_y, wanted_max_y, title_override, force_range);
      ret.populate(populator, cumulative, parent->events.size());
      ret.set_style(parent->Styles[int(style_ref)]);
      return ret;
    }
                                        
    virtual group_T create_group(const double wanted_min_x, const double wanted_max_x, 
				 const std::string& title_override = "",
				 const bool force_range = false) const 
    {
      group_T ret = this->construct_group(wanted_min_x, wanted_max_x, title_override, force_range);
      ret.populate(populator, cumulative, parent->events.size());
      ret.set_style(parent->Styles[int(style_ref)]);
      return ret;
    }
    
    
    virtual group_T construct_group(const std::string& title_override = "") const = 0;
    
    virtual group_T construct_group(const std::array<double, 4> &wanted_min_x,
				    const std::array<double, 4> &wanted_max_x,
				    const std::array<double, 4> &wanted_min_y,
				    const std::array<double, 4> &wanted_max_y,
				    const std::string& title_override = "",
				    const bool force_range = false) const = 0;
                                        
    virtual group_T construct_group(const std::array<double, 4> &wanted_min_x,
				    const std::array<double, 4> &wanted_max_x,
				    const std::string& title_override = "",
				    const bool force_range = false) const = 0;
    
    
    virtual group_T construct_group(const double wanted_min_x, const double wanted_max_x,
				    const double wanted_min_y, const double wanted_max_y,
				    const std::string& title_override = "",
				    const bool force_range = false) const
    {
      return this->construct_group(std::array<double, 4>{wanted_min_x, wanted_min_x, wanted_min_x, wanted_min_x},
				   std::array<double, 4>{wanted_max_x, wanted_max_x, wanted_max_x, wanted_max_x},
				   std::array<double, 4>{wanted_min_y, wanted_min_y, wanted_min_y, wanted_min_y},
				   std::array<double, 4>{wanted_max_y, wanted_max_y, wanted_max_y, wanted_max_y},
				   title_override, force_range);
    }
                                        
    virtual group_T construct_group(const double wanted_min_x, const double wanted_max_x, 
				    const std::string& title_override = "",
				    const bool force_range = false) const
    {
      return this->construct_group(std::array<double, 4>{wanted_min_x, wanted_min_x, wanted_min_x, wanted_min_x},
				   std::array<double, 4>{wanted_max_x, wanted_max_x, wanted_max_x, wanted_max_x},
				   title_override, force_range);
    }
    
    
    virtual void plot(const std::string &path, const std::string& prefix = "", const std::string& suffix = "") const
    {
      group_T group = this->create_group();
      this->save(path + "/" + prefix + hist_name + suffix, group);
    }
    
    virtual void plot(const std::array<double, 4> &wanted_min_x, const std::array<double, 4> &wanted_max_x,
		      const std::string &path, const std::string& prefix = "", const std::string& suffix = "") const
    {
      group_T group = this->create_group(wanted_min_x, wanted_max_x);
      this->save(path + "/" + prefix + hist_name + suffix, group);
    }
                      
    virtual void plot(const std::array<double, 4> &wanted_min_x, const std::array<double, 4> &wanted_max_x,
		      const std::array<double, 4> &wanted_min_y, const std::array<double, 4> &wanted_max_y,
		      const std::string &path, const std::string& prefix = "", const std::string& suffix = "") const
    {
      group_T group = this->create_group(wanted_min_x, wanted_max_x, wanted_min_y, wanted_max_y);
      this->save(path + "/" + prefix + hist_name + suffix, group);
    }
    
  };
  
  struct H1D_plotter : public basic_plotter<hist_group_1D>
  {
    std::function<double(int)> min_calc;
    std::function<double(int)> max_calc;
    template <class F1, class F2, class F3>
    H1D_plotter(const ClusterPlotter *p, const std::string &name, F1 && minc, F2 && maxc, const bool cbn, const int cml,
                const std::string & title, const std::string & xlbl, const std::string & ylbl,
                F3 && popl, const StyleKinds &stl)
    {
      hist_name = name;
      hist_title = title;
      x_label = xlbl;
      y_label = ylbl;
      parent = p;
      can_be_negative = cbn;
      cumulative = cml;
      style_ref = stl;
      populator = popl;
      min_calc = minc;
      max_calc = maxc;
    }
        
    void calc_data_range(std::array<double, 4> & min, std::array<double, 4> & max) const
    {
      for (int i = 0; i < 4; ++i)
        {
          const double data_min = min_calc(i);
          const double data_max = max_calc(i);
          
          const double data_range = data_max - data_min;
          
          min[i] = ( can_be_negative ?
                     data_min - 0.05 * data_range :
                     std::max(data_min - 0.05 * data_range, -0.5) );
          max[i] = data_max + 0.05 * data_range;
        }
    }
    
    void calc_data_range(std::array<double, 4> & min, std::array<double, 4> & max,
                         const std::array<double, 4> & wanted_min, const std::array<double, 4> & wanted_max) const
    {
      for (int i = 0; i < 4; ++i)
        {
          if ( (wanted_min[i] <= -1 && !can_be_negative) || wanted_max[i] < 0)
            {
              const double data_min = min_calc(i);
              const double data_max = max_calc(i);
              
              const double data_range = data_max - data_min;
              
              min[i] = ( can_be_negative ?
                         data_min - 0.05 * data_range :
                         std::max(data_min - 0.05 * data_range, -0.5) );
              max[i] = data_max + 0.05 * data_range;
            }
          else
            {
              min[i] = wanted_min[i];
              max[i] = wanted_max[i];
            }
        }
    }
  };
  
  struct H1D_plotter_type : public H1D_plotter
  {
    PlotterKind plotter_kind() const { return PlotterKind::type; }

    using H1D_plotter::H1D_plotter;
    
    hist_group_1D construct_group(const std::string& title_override = "") const
    {
      std::array<double, 4> min, max;
      
      calc_data_range(min, max);
      
      if (title_override == "")
        {
          return hist_group_1D(min, max, parent->num_bins, hist_name, hist_title,
			       x_label, y_label, parent->label_type_1, parent->label_type_2, parent->label_type_3);
        }
      else
        {
          return hist_group_1D(min, max, parent->num_bins, hist_name, title_override);
        }
    }
    
    hist_group_1D construct_group(const std::array<double, 4> &wanted_min, const std::array<double, 4> &wanted_max,
                                  const std::string& title_override = "",
				  const bool force_range = false) const
    {      
      std::array<double, 4> min, max;
      
      if (force_range)
	{
	  for (int i = 0; i < 4; ++i)
	    {
	      min[i] = wanted_min[i];
	      max[i] = wanted_max[i];
	    }
	}
      else
	{
	  calc_data_range(min, max, wanted_min, wanted_max);
	}
      
      if (title_override == "")
        {
          return hist_group_1D(min, max, parent->num_bins, hist_name, hist_title,
			       x_label, y_label, parent->label_type_1, parent->label_type_2, parent->label_type_3);
        }
      else
        {
          return hist_group_1D(min, max, parent->num_bins, hist_name, title_override);
        }
    }
    
    hist_group_1D construct_group(const std::array<double, 4> &min, const std::array<double, 4> &max,
				  const std::array<double, 4> &ignore1, const std::array<double, 4> &ignore2,
                                  const std::string& title_override = "",
				  const bool force_range = false) const
    {      
      return this->construct_group(min, max, title_override, force_range);
    }
    
    void save(const std::string & base_name, const hist_group_1D & group) const
    {
      TCanvas cv("cv", "canvas", parent->canvas_x, parent->canvas_y);
      plot_one(&cv, group.global.get(), base_name, parent->file_extensions, parent->print_options);
      plot_one(&cv, group.t1.get(), base_name + "_" + parent->suffix_type_1, parent->file_extensions, parent->print_options);
      plot_one(&cv, group.t2.get(), base_name + "_" + parent->suffix_type_2, parent->file_extensions, parent->print_options);
      plot_one(&cv, group.t3.get(), base_name + "_" + parent->suffix_type_3, parent->file_extensions, parent->print_options);
    }
  };
  
  struct H1D_plotter_region : public H1D_plotter
  {
    PlotterKind plotter_kind() const { return PlotterKind::region; }

    using H1D_plotter::H1D_plotter;
    
    hist_group_1D construct_group(const std::string& title_override = "") const
    {
      std::array<double, 4> min, max;
      
      calc_data_range(min, max);
      
      if (title_override == "")
        {
          return hist_group_1D(min, max, parent->num_bins, hist_name, hist_title, x_label, y_label,
			       parent->label_region_1, parent->label_region_2, parent->label_region_3);
        }
      else
        {
          return hist_group_1D(min, max, parent->num_bins, hist_name, title_override);
        }
    }
    
    hist_group_1D construct_group(const std::array<double, 4> &wanted_min, const std::array<double, 4> &wanted_max,
                                  const std::string& title_override = "",
				  const bool force_range = false) const
    {      
      std::array<double, 4> min, max;
      
      if (force_range)
	{
	  for (int i = 0; i < 4; ++i)
	    {
	      min[i] = wanted_min[i];
	      max[i] = wanted_max[i];
	    }
	}
      else
	{
	  calc_data_range(min, max, wanted_min, wanted_max);
	}
      
      if (title_override == "")
        {
          return hist_group_1D(min, max, parent->num_bins, hist_name, hist_title, x_label, y_label,
			       parent->label_region_1, parent->label_region_2, parent->label_region_3);
        }
      else
        {
          return hist_group_1D(min, max, parent->num_bins, hist_name, title_override);
        }
    }
    
    hist_group_1D construct_group(const std::array<double, 4> &min, const std::array<double, 4> &max,
				  const std::array<double, 4> &ignore1, const std::array<double, 4> &ignore2,
				  const std::string& title_override = "",
				  const bool force_range = false) const
    {      
      return this->construct_group(min, max, title_override, force_range);
    }
    
    void save(const std::string & base_name, const hist_group_1D &group) const
    {
      TCanvas cv("cv", "canvas", parent->canvas_x, parent->canvas_y);
      plot_one(&cv, group.global.get(), base_name, parent->file_extensions, parent->print_options);
      plot_one(&cv, group.t1.get(), base_name + "_" + parent->suffix_region_1, parent->file_extensions, parent->print_options);
      plot_one(&cv, group.t2.get(), base_name + "_" + parent->suffix_region_2, parent->file_extensions, parent->print_options);
      plot_one(&cv, group.t3.get(), base_name + "_" + parent->suffix_region_3, parent->file_extensions, parent->print_options);
    }
  };  
  

  struct H1D_plotter_time : public H1D_plotter
  {
    PlotterKind plotter_kind() const { return PlotterKind::time; }

    using H1D_plotter::H1D_plotter;
    
    hist_group_1D construct_group(const std::string& title_override = "") const
    {
      std::array<double, 4> min, max;
      
      calc_data_range(min, max);
      
      if (title_override == "")
        {
          return hist_group_1D(min, max, parent->num_bins, hist_name, hist_title,
			       x_label, y_label, "", "", "");
        }
      else
        {
          return hist_group_1D(min, max, parent->num_bins, hist_name, title_override);
        }
    }
    
    hist_group_1D construct_group(const std::array<double, 4> &wanted_min, const std::array<double, 4> &wanted_max,
                                  const std::string& title_override = "",
				  const bool force_range = false) const
    {      
      std::array<double, 4> min, max;
      
      if (force_range)
	{
	  for (int i = 0; i < 4; ++i)
	    {
	      min[i] = wanted_min[i];
	      max[i] = wanted_max[i];
	    }
	}
      else
	{
	  calc_data_range(min, max, wanted_min, wanted_max);
	}
      
      if (title_override == "")
        {
          return hist_group_1D(min, max, parent->num_bins, hist_name, hist_title,
			       x_label, y_label, "", "", "");
        }
      else
        {
          return hist_group_1D(min, max, parent->num_bins, hist_name, title_override);
        }
    }
    
    hist_group_1D construct_group(const std::array<double, 4> &min, const std::array<double, 4> &max,
				  const std::array<double, 4> &ignore1, const std::array<double, 4> &ignore2,
                                  const std::string& title_override = "",
				  const bool force_range = false) const
    {      
      return this->construct_group(min, max, title_override, force_range);
    }
    
    void save(const std::string & base_name, const hist_group_1D & group) const
    {
      TCanvas cv("cv", "canvas", parent->canvas_x, parent->canvas_y);
      plot_one(&cv, group.global.get(), base_name, parent->file_extensions, parent->print_options);
    }
  };

  struct H2D_plotter : public basic_plotter<hist_group_2D>
  {
    std::function<double(int)> min_calc_x;
    std::function<double(int)> max_calc_x;
    std::function<double(int)> min_calc_y;
    std::function<double(int)> max_calc_y;
    template <class F1, class F2, class F3, class F4, class F5>
    H2D_plotter(const ClusterPlotter *p, const std::string &name, F1 && mincx, F2 && maxcx, F3 && mincy, F4 && maxcy,
                const bool cbn, const std::string & title, const std::string & xlbl, const std::string & ylbl,
                F5 && popl, const StyleKinds &stl)
    {
      hist_name = name;
      hist_title = title;
      x_label = xlbl;
      y_label = ylbl;
      parent = p;
      can_be_negative = cbn;
      style_ref = stl;
      populator = popl;
      min_calc_x = mincx;
      max_calc_x = maxcx;
      min_calc_y = mincy;
      max_calc_y = maxcy;
    }
        
    void calc_data_range_x(std::array<double, 4> & min, std::array<double, 4> & max) const
    {
      for (int i = 0; i < 4; ++i)
        {
          const double data_min = min_calc_x(i);
          const double data_max = max_calc_x(i);
          
          const double data_range = data_max - data_min;
          
          min[i] = ( can_be_negative ?
                     data_min - 0.05 * data_range :
                     std::max(data_min - 0.05 * data_range, -0.5) );
          max[i] = data_max + 0.05 * data_range;
        }
    }
    
    void calc_data_range_x(std::array<double, 4> & min, std::array<double, 4> & max,
                           const std::array<double, 4> & wanted_min, const std::array<double, 4> & wanted_max) const
    {
      for (int i = 0; i < 4; ++i)
        {
          if ( (wanted_min[i] <= -1 && !can_be_negative) || wanted_max[i] < 0)
            {
              const double data_min = min_calc_x(i);
              const double data_max = max_calc_x(i);
              
              const double data_range = data_max - data_min;
              
              min[i] = ( can_be_negative ?
                         data_min - 0.05 * data_range :
                         std::max(data_min - 0.05 * data_range, -0.5) );
              max[i] = data_max + 0.05 * data_range;
            }
          else
            {
              min[i] = wanted_min[i];
              max[i] = wanted_max[i];
            }
        }
    }
    
    void calc_data_range_y(std::array<double, 4> & min, std::array<double, 4> & max) const
    {
      for (int i = 0; i < 4; ++i)
        {
          const double data_min = min_calc_y(i);
          const double data_max = max_calc_y(i);
          
          const double data_range = data_max - data_min;
          
          min[i] = ( can_be_negative ?
                     data_min - 0.05 * data_range :
                     std::max(data_min - 0.05 * data_range, -0.5) );
          max[i] = data_max + 0.05 * data_range;
        }
    }
    
    void calc_data_range_y(std::array<double, 4> & min, std::array<double, 4> & max,
                           const std::array<double, 4> & wanted_min, const std::array<double, 4> & wanted_max) const
    {
      for (int i = 0; i < 4; ++i)
        {
          if ( (wanted_min[i] <= -1 && !can_be_negative) || wanted_max[i] < 0)
            {
              const double data_min = min_calc_y(i);
              const double data_max = max_calc_y(i);
              
              const double data_range = data_max - data_min;
              
              min[i] = ( can_be_negative ?
                         data_min - 0.05 * data_range :
                         std::max(data_min - 0.05 * data_range, -0.5) );
              max[i] = data_max + 0.05 * data_range;
            }
          else
            {
              min[i] = wanted_min[i];
              max[i] = wanted_max[i];
            }
        }
    }
  };
  
  struct H2D_plotter_type : public H2D_plotter
  {
    PlotterKind plotter_kind() const { return PlotterKind::type2D; }

    using H2D_plotter::H2D_plotter;
    
    hist_group_2D construct_group(const std::string& title_override = "") const
    {
      std::array<double, 4> min_x, max_x, min_y, max_y;
      
      calc_data_range_x(min_x, max_x);
      calc_data_range_y(min_y, max_y);
      
      return hist_group_2D(min_x, max_x, min_y, max_y, parent->num_bins, hist_name, hist_title, x_label, y_label,
			   parent->label_type_1, parent->label_type_2, parent->label_type_3);
    }
    
    hist_group_2D construct_group(const std::array<double, 4> &wanted_min, const std::array<double, 4> &wanted_max, 
				  const std::string& title_override = "",
				  const bool force_range = false) const
    {      
      std::array<double, 4> min_x, max_x, min_y, max_y;
      
      if (force_range)
	{
	  for (int i = 0; i < 4; ++i)
	    {
	      min_x[i] = wanted_min[i];
	      max_x[i] = wanted_max[i];
	    }
	}
      else
	{
	  calc_data_range_x(min_x, max_x, wanted_min, wanted_max);
	}

      calc_data_range_y(min_y, max_y);
      
      return hist_group_2D(min_x, max_x, min_y, max_y, parent->num_bins, hist_name, hist_title, x_label, y_label,
                           parent->label_type_1, parent->label_type_2, parent->label_type_3);
    }
    
    hist_group_2D construct_group(const std::array<double, 4> &wanted_min_x, const std::array<double, 4> &wanted_max_x,
                                  const std::array<double, 4> &wanted_min_y, const std::array<double, 4> &wanted_max_y,
				  const std::string& title_override = "",
				  const bool force_range = false) const
    {      
      std::array<double, 4> min_x, max_x, min_y, max_y;
      
      if (force_range)
	{
	  for (int i = 0; i < 4; ++i)
	    {
	      min_x[i] = wanted_min_x[i];
	      max_x[i] = wanted_max_x[i];
	      min_y[i] = wanted_min_y[i];
	      max_y[i] = wanted_max_y[i];
	    }
	}
      else
	{
	  calc_data_range_x(min_x, max_x, wanted_min_x, wanted_max_x);
	  calc_data_range_y(min_y, max_y, wanted_min_y, wanted_max_y);
	}
      
      return hist_group_2D(min_x, max_x, min_y, max_y, parent->num_bins, hist_name, hist_title, x_label, y_label,
                           parent->label_type_1, parent->label_type_2, parent->label_type_3);
    }
    
    void save(const std::string & base_name, const hist_group_2D &group) const
    {
      TCanvas cv("cv", "canvas", parent->canvas_x, parent->canvas_y);
      plot_one(&cv, group.global.get(), base_name, parent->file_extensions, parent->print_options);
      plot_one(&cv, group.t1.get(), base_name + "_" + parent->suffix_type_1, parent->file_extensions, parent->print_options);
      plot_one(&cv, group.t2.get(), base_name + "_" + parent->suffix_type_2, parent->file_extensions, parent->print_options);
      plot_one(&cv, group.t3.get(), base_name + "_" + parent->suffix_type_3, parent->file_extensions, parent->print_options);
    }
  };
  

  struct H2D_plotter_region : public H2D_plotter
  {
    PlotterKind plotter_kind() const { return PlotterKind::region2D; }

    using H2D_plotter::H2D_plotter;
    
    hist_group_2D construct_group(const std::string& title_override = "") const
    {
      std::array<double, 4> min_x, max_x, min_y, max_y;
      
      calc_data_range_x(min_x, max_x);
      calc_data_range_y(min_y, max_y);
      
      return hist_group_2D(min_x, max_x, min_y, max_y, parent->num_bins, hist_name, hist_title, x_label, y_label,
                           parent->label_region_1, parent->label_region_2, parent->label_region_3);
    }
    
    hist_group_2D construct_group(const std::array<double, 4> &wanted_min, const std::array<double, 4> &wanted_max,
				  const std::string& title_override = "",
				  const bool force_range = false) const
    {      
      std::array<double, 4> min_x, max_x, min_y, max_y;
      
      if (force_range)
	{
	  for (int i = 0; i < 4; ++i)
	    {
	      min_x[i] = wanted_min[i];
	      max_x[i] = wanted_max[i];
	    }
	}
      else
	{
	  calc_data_range_x(min_x, max_x, wanted_min, wanted_max);
	}

      calc_data_range_y(min_y, max_y);
      
      return hist_group_2D(min_x, max_x, min_y, max_y, parent->num_bins, hist_name, hist_title, x_label, y_label,
                           parent->label_region_1, parent->label_region_2, parent->label_region_3);
    }
    
    hist_group_2D construct_group(const std::array<double, 4> &wanted_min_x, const std::array<double, 4> &wanted_max_x,
                                  const std::array<double, 4> &wanted_min_y, const std::array<double, 4> &wanted_max_y,
				  const std::string& title_override = "",
				  const bool force_range = false) const
    {      
      std::array<double, 4> min_x, max_x, min_y, max_y;
      
      if (force_range)
	{
	  for (int i = 0; i < 4; ++i)
	    {
	      min_x[i] = wanted_min_x[i];
	      max_x[i] = wanted_max_x[i];
	      min_y[i] = wanted_min_y[i];
	      max_y[i] = wanted_max_y[i];
	    }
	}
      else
	{
	  calc_data_range_x(min_x, max_x, wanted_min_x, wanted_max_x);
	  calc_data_range_y(min_y, max_y, wanted_min_y, wanted_max_y);
	}
      
      return hist_group_2D(min_x, max_x, min_y, max_y, parent->num_bins, hist_name, hist_title, x_label, y_label,
                           parent->label_region_1, parent->label_region_2, parent->label_region_3);
    }
    
    void save(const std::string & base_name, const hist_group_2D &group) const
    {
      TCanvas cv("cv", "canvas", parent->canvas_x, parent->canvas_y);
      plot_one(&cv, group.global.get(), base_name, parent->file_extensions, parent->print_options);
      plot_one(&cv, group.t1.get(), base_name + "_" + parent->suffix_type_1, parent->file_extensions, parent->print_options);
      plot_one(&cv, group.t2.get(), base_name + "_" + parent->suffix_type_2, parent->file_extensions, parent->print_options);
      plot_one(&cv, group.t3.get(), base_name + "_" + parent->suffix_type_3, parent->file_extensions, parent->print_options);
    }
  };

  struct joined_plotter : public plotter_base
  {
    std::vector<const H1D_plotter *> parts;
    std::vector<std::string> labels;
    
    void add_plot()
    {
    }

    void add_plot(const plotter_base *pplt, const std::string &label)
    {
      const H1D_plotter * conv_1 = dynamic_cast<const H1D_plotter *>(pplt);
      const joined_plotter * conv_2 = dynamic_cast<const joined_plotter *>(pplt);
      if (conv_1 != nullptr)
        {
          parts.push_back(conv_1);
          labels.push_back(label);
	  //std::cout << "Added " << pplt->hist_name << " as " << label << std::endl;
        }
      else if (conv_2 != nullptr)
	{
	  for (int i = 0; i < conv_2->parts.size(); ++i)
	    {
	      parts.push_back(conv_2->parts[i]);
	      labels.push_back(label + ": " + conv_2->labels[i]);
	    }
	  //std::cout << "Added all from " << pplt->hist_name << " as " << label << std::endl;
	}
      else
        {
          std::cout << "WARNING: Trying to add to plot '" << hist_name 
                    << "' an invalid plot: '" << pplt->hist_name << "' (labelled '" << label << "')" << std::endl;
        }
    }

    template <class ... Rest>
    void add_plot(const plotter_base *pplt, const std::string& label, Rest && ... rest)
    {
      add_plot(pplt, label);
      add_plot(std::forward<Rest>(rest)...);
    }

    joined_plotter(const ClusterPlotter * p, const std::string & name, const std::string & title, const std::string & xlbl, const std::string & ylbl)
    {
      hist_name = name;
      hist_title = title;
      x_label = xlbl;
      y_label = ylbl;
      parent = p;
    }
    
    template <class ... Rest>
    joined_plotter(const ClusterPlotter * p, const std::string & name, const std::string & title,
		   const std::string & xlbl, const std::string & ylbl, Rest && ... rest):
      joined_plotter(p, name, title, xlbl, ylbl)
    {
      add_plot(std::forward<Rest>(rest)...);
    }
    
    virtual hist_stacker create_stack() const = 0;
    virtual void save(const std::string & base_name, const hist_stacker & stacker) const = 0;
    
  protected:
    void do_plots(const std::array<double, 4> &min_x, const std::array<double, 4> &max_x,
                      const std::string &path, const std::string& prefix = "", const std::string& suffix = "") const
    {
      std::vector<hist_group_1D> hists;
      hist_stacker stacker = this->create_stack();
      
      hists.reserve(parts.size());

      for(int i = 0; i < parts.size(); ++i)
	{
	  hists.push_back(parts[i]->create_group(min_x, max_x, labels[i], true));
	  hists.back().add_to_stack(stacker);
	}
      
      this->save(path + "/" + prefix + hist_name + suffix, stacker);
    }

  public:


    virtual void plot(const std::string &path, const std::string& prefix = "", const std::string& suffix = "") const
    {
      std::array<double, 4> real_min_x;
      std::array<double, 4> real_max_x;

      for (int j = 0; j < 4; ++j)
	{
	  set_to_highest(real_min_x[j]);
	  set_to_lowest(real_max_x[j]);
	}

      for (int i = 0; i < parts.size(); ++i)
	{
	  std::array<double, 4> temp_min_x, temp_max_x;
	  parts[i]->calc_data_range(temp_min_x, temp_max_x);
	  for (int j = 0; j < 4; ++j)
	    {
	      real_min_x[j] = std::min(real_min_x[j], temp_min_x[j]);
	      real_max_x[j] = std::max(real_max_x[j], temp_max_x[j]);
	    }
	}
      
      do_plots(real_min_x, real_max_x, path, prefix, suffix);
    }

    virtual void plot(const std::array<double, 4> &wanted_min_x, const std::array<double, 4> &wanted_max_x,
                      const std::string &path, const std::string& prefix = "", const std::string& suffix = "") const
    {
      std::array<double, 4> real_min_x;
      std::array<double, 4> real_max_x;

      for (int j = 0; j < 4; ++j)
	{
	  set_to_highest(real_min_x[j]);
	  set_to_lowest(real_max_x[j]);
	}

      for (int i = 0; i < parts.size(); ++i)
	{
	  std::array<double, 4> temp_min_x, temp_max_x;
	  parts[i]->calc_data_range(temp_min_x, temp_max_x, wanted_min_x, wanted_max_x);
	  for (int j = 0; j < 4; ++j)
	    {
	      real_min_x[j] = std::min(real_min_x[j], temp_min_x[j]);
	      real_max_x[j] = std::max(real_max_x[j], temp_max_x[j]);
	    }
	}
      
      do_plots(real_min_x, real_max_x, path, prefix, suffix);
    }

    virtual void plot(const std::array<double, 4> &wanted_min_x, const std::array<double, 4> &wanted_max_x,
                      const std::array<double, 4> &ignore1, const std::array<double, 4> &ignore2,
                      const std::string &path, const std::string& prefix = "", const std::string& suffix = "") const
    {
      this->plot(wanted_min_x, wanted_max_x, path, prefix, suffix);
    }
    
  };
  
  struct joined_plotter_type : public joined_plotter
  {
    PlotterKind plotter_kind() const { return PlotterKind::type; }

    using joined_plotter::joined_plotter;
    
    virtual hist_stacker create_stack() const
    {
      return hist_stacker(hist_name, hist_title, x_label, y_label, parent->label_type_1, parent->label_type_2, parent->label_type_3);
    }
    
    virtual void save(const std::string & base_name, const hist_stacker & stacker) const
    {
      TCanvas cv("cv", "canvas", parent->canvas_x, parent->canvas_y);
      plot_one(&cv, stacker.global.get(), base_name, parent->file_extensions, parent->print_options);
      plot_one(&cv, stacker.t1.get(), base_name + "_" + parent->suffix_type_1, parent->file_extensions, parent->print_options);
      plot_one(&cv, stacker.t2.get(), base_name + "_" + parent->suffix_type_2, parent->file_extensions, parent->print_options);
      plot_one(&cv, stacker.t3.get(), base_name + "_" + parent->suffix_type_3, parent->file_extensions, parent->print_options);
    }
  };
  
  struct joined_plotter_region : public joined_plotter
  {
    PlotterKind plotter_kind() const { return PlotterKind::region; }

    using joined_plotter::joined_plotter;
    
    virtual hist_stacker create_stack() const
    {
      return hist_stacker(hist_name, hist_title, x_label, y_label, parent->label_region_1, parent->label_region_2, parent->label_region_3);
    }
    
    virtual void save(const std::string & base_name, const hist_stacker & stacker) const
    {
      TCanvas cv("cv", "canvas", parent->canvas_x, parent->canvas_y);
      plot_one(&cv, stacker.global.get(), base_name, parent->file_extensions, parent->print_options);
      plot_one(&cv, stacker.t1.get(), base_name + "_" + parent->suffix_region_1, parent->file_extensions, parent->print_options);
      plot_one(&cv, stacker.t2.get(), base_name + "_" + parent->suffix_region_2, parent->file_extensions, parent->print_options);
      plot_one(&cv, stacker.t3.get(), base_name + "_" + parent->suffix_region_3, parent->file_extensions, parent->print_options);
    }
  };
  
  struct joined_plotter_time : public joined_plotter
  {
    PlotterKind plotter_kind() const { return PlotterKind::time; }

    using joined_plotter::joined_plotter;
    
    virtual hist_stacker create_stack() const
    {
      return hist_stacker(hist_name, hist_title, x_label, y_label, "", "", "");
    }
    
    virtual void save(const std::string & base_name, const hist_stacker & stacker) const
    {
      TCanvas cv("cv", "canvas", parent->canvas_x, parent->canvas_y);
      plot_one(&cv, stacker.global.get(), base_name, parent->file_extensions, parent->print_options);
    }
  };

  std::unordered_map<std::string, std::unique_ptr<plotter_base>> plots;
    
  template <class T, class ... Args>
  plotter_base * add_plot(Args && ... args)
  {
    std::unique_ptr<plotter_base> ptr = std::make_unique<T>(this, std::forward<Args>(args)...);
    auto it = plots.try_emplace(ptr->hist_name, std::move(ptr));
    //std::cout << "Added plot '" << it.first->second->hist_name << "'" << std::endl;
    return it.first->second.get();
  }

private:
  void populate_plots();
public:
  
  template <class Graph, class ... Args>
  void fill_regions(Graph *all, Graph *central, Graph *endcap, Graph *forward, const float eta, Args && ... args) const
  {

    all->Fill(std::forward<Args>(args)...);

    const float abs_eta = std::abs(eta);
    if (abs_eta < regCuts[0])
      {
	central->Fill(std::forward<Args>(args)...);
      }
    else if (abs_eta < regCuts[1])
      {
	endcap->Fill(std::forward<Args>(args)...);
      }
    else if (abs_eta < regCuts[2])
      {
	forward->Fill(std::forward<Args>(args)...);
      }
    else
      {
	//std::cout << "ERROR: " << abs_eta << " is outside expected eta parameters!" << std::endl;
      }
  }

  template <class Graph, class ... Args>
  void fill_types(Graph *all, Graph *seed, Graph *grow, Graph *terminal, const float SNR, Args && ... args) const
  {

    all->Fill(std::forward<Args>(args)...);

    if (SNR > SNR_thresholds[2])
      {
	seed->Fill(std::forward<Args>(args)...);
      }
    else if (SNR > SNR_thresholds[1])
      {
	grow->Fill(std::forward<Args>(args)...);
      }
    else if (SNR > SNR_thresholds[0])
      {
	terminal->Fill(std::forward<Args>(args)...);
      }
    else
      {
	//std::cout << "ERROR: " << abs_eta << " is outside expected eta parameters!" << std::endl;
      }
  }

  
  template <class Graph, class ... Args>
  void fill_regions(hist_group<Graph> *group, const float x, Args && ... args) const
  {
    fill_regions(group->global.get(), group->t1.get(), group->t2.get(), group->t3.get(), x, std::forward<Args>(args)...);
  }

  template <class Graph, class ... Args>
  void fill_types(hist_group<Graph> *group, const float x, Args && ... args) const
  {
    fill_types(group->global.get(), group->t1.get(), group->t2.get(), group->t3.get(), x, std::forward<Args>(args)...);
  }

  template <class ... Args>
  void plot(const std::string & name, Args && ... args) const
  {
    const auto it = plots.find(name);
    if (it != plots.end())
      {
	it->second->plot(std::forward<Args>(args)...);
      }
    else
      {
	std::cout << "ERROR: Trying to plot non-existent plot: '" << name << "'" << std::endl;
      }
  }

private:

  void load_single_file(const std::filesystem::path & filename, std::vector<std::vector<double>> &times, std::vector<std::string> &text)
  {
    std::ifstream is(filename);
    if (!is.is_open())
      {
	std::cout << "ERROR: Could not open '" << filename << "' to load times!" << std::endl;
	return;
      }
    else
      {
	std::cout << "INFO: Loading times from '" << filename << "'." << std::endl;
      }

    while(is.good() && is.peek() != '\n')
      {
	std::string temp;
	is >> temp;
	//This reads space-separated columns,
	//which is exactly what we want.

	if (temp == "Event_Number")
	  {
	    continue;
	  }

	for (auto & x: temp)
	  //Replace underscores by spaces to get the intended names...
	  {
	    if (x == '_')
	      {
		x = ' ';
	      }
	  }

	text.push_back(temp);
      }

    times.resize(text.size());

    is.get();
      
    while(is.get() != '\n')
      {
	continue;
      }
    //Exclude the first event: setup-time delays and so on.

    while(is.good())
      {
	int count = 0;
	while(is.good() && is.peek() != '\n')
	  {
	    double temp = 0.;
	    is >> temp;
	    if (is.fail())
	      {
		break;
	      }
	    if (count > 0)
	      {
		times[count-1].push_back(temp);
	      }
	    //The first element is the event number...
	    ++count;
	  }
	is.get();
      }
    is.close();
  }

  void check_time_outliers(std::set<int> & to_remove, const std::vector<double> & col, const double factor, const int ignore_largest)
  { 
    if (col.size() < 1)
      {
	std::cout << "ERROR: Trying to remove outliers from empty time column!" << std::endl;
	return;
      }

    std::vector<double> copy_col = col;

    std::sort(copy_col.begin(), copy_col.end());

    const int ignore_limit = std::min(ignore_largest, int(copy_col.size()/4));
    

    double mu = copy_col[0];

    for (int i = 1; i < copy_col.size() - ignore_limit; ++i)
      {
	mu += copy_col[i];
      }

    mu /= std::max(int(copy_col.size() - ignore_limit), 1);
	
    const double upper_limit = (mu - copy_col[0]) * factor + mu;

    for (int i = 0; i < col.size(); ++i)
      {
	if (col[i] > upper_limit)
	  {
	    to_remove.insert(i);
	  }
      }
  }


  void reorder_to_exclude_outliers(std::vector<double> & col, const std::set<int> & to_remove)
  {    
    int last_valid = col.size() - 1;
    const auto stop = to_remove.crend();
    for (auto it = to_remove.crbegin(); it != stop; ++it)
      {
	col[*it] = col[last_valid];
	last_valid--;
      }
    col.resize(col.size() - to_remove.size());
  }

  void remove_time_outliers(std::vector<std::vector<double>> &times_1, std::vector<std::vector<double>> &times_2, const double factor = 20., const int ignore_largest = 30)
  {
    
    std::set<int> to_remove;

    for (const auto & col: times_1)
      {
	check_time_outliers(to_remove, col, factor, ignore_largest);
      }

    for (const auto & col: times_2)
      {
	check_time_outliers(to_remove, col, factor, ignore_largest);
      }

    for (auto & col: times_1)
      {
	reorder_to_exclude_outliers(col, to_remove);
      }

    for (auto & col: times_2)
      {
	reorder_to_exclude_outliers(col, to_remove);
      }

    std::cout << "INFO: Removed " << to_remove.size() << " outliers from times." << std::endl;

  }
public:


  std::vector<std::vector<double>> reference_time_columns;
  std::vector<std::string> reference_time_column_text;

  std::vector<std::vector<double>> modified_time_columns;
  std::vector<std::string> modified_time_column_text;

  std::vector<double> max_reference_time, min_reference_time, max_modified_time, min_modified_time;

private:

  void load_times(const std::filesystem::path & reference_times,
		  const std::filesystem::path & modified_times)
  {

    load_single_file(reference_times, reference_time_columns, reference_time_column_text);

    load_single_file(modified_times, modified_time_columns, modified_time_column_text);


    remove_time_outliers(reference_time_columns, modified_time_columns);

    {
      std::cout << "INFO: Semicolon-separated modified time list:\nColumn;";
      for (const auto& text : modified_time_column_text)
	{
	  std::cout << text << ";";
	}
      std::cout << "\nAverage (us);";
      std::vector<double> vars;
      vars.reserve(modified_time_columns.size());
      for (const auto& vec : modified_time_columns)
	{
	  double avg = 0;
	  double sqavg = 0;
	  for (const auto& val: vec)
	    {
	      avg += val;
	      sqavg += val*val;
	    }
	  avg /= vec.size();
	  std::cout << avg << ";";
	  vars.push_back(std::sqrt(sqavg/vec.size() - avg * avg));
	}
      std::cout << "\nVariance (us)";
      for (const auto t : vars)
	{
	  std::cout << t << ";";
	}
      std::cout << "\n\n" << std::endl;
    }

    for(const auto &v : reference_time_columns)
      {
	double temp_min, temp_max;
	set_to_lowest(temp_max);
	set_to_highest(temp_min);
	for (const auto val : v)
	  {
	    temp_max = std::max(temp_max, val);
	    temp_min = std::min(temp_min, val);
	  }
	max_reference_time.push_back(temp_max);
	min_reference_time.push_back(temp_min);
      }

    for(const auto &vec : modified_time_columns)
      {
	double temp_min, temp_max;
	set_to_lowest(temp_max);
	set_to_highest(temp_min);
	for (const auto & val : vec)
	  {
	    temp_max = std::max(temp_max, val);
	    temp_min = std::min(temp_min, val);
	  }
	max_modified_time.push_back(temp_max);
	min_modified_time.push_back(temp_min);
      }

#ifndef RETFUNC_WITH_IDX
#define RETFUNC_WITH_IDX(...) [&, i](const int j){ return double(__VA_ARGS__); }
#endif
    //We capture i by value in the lambdas because it is a local variable!

    for (int i = 0; i < reference_time_columns.size(); ++i)
      {
	add_plot<H1D_plotter_time>(
				   std::string("time_reference_") + std::to_string(i),
				   RETFUNC_WITH_IDX(min_reference_time[i]),
				   RETFUNC_WITH_IDX(max_reference_time[i]),
				   false, 0, reference_time_column_text[i], "#font[52]{t} [#mus}", (normalize ? "Fraction of Events" : "Events"),
				   [&, i](hist_group_1D *group)
				   {
				     for (const auto num : reference_time_columns[i])
				       {
			 
					 group->global->Fill(num);
				       }
		     
				   },
				   StyleKinds::ref);
      }

    for (int i = 0; i < modified_time_columns.size(); ++i)
      {
	add_plot<H1D_plotter_time>(
				   std::string("time_modified_") + std::to_string(i),
				   RETFUNC_WITH_IDX(min_modified_time[i]),
				   RETFUNC_WITH_IDX(max_modified_time[i]),
				   false, 0, modified_time_column_text[i], "#font[52]{t} [#mus]", (normalize ? "Fraction of Events" : "Events"),
				   [&, i](hist_group_1D *group)
				   {
				     for (const auto num : modified_time_columns[i])
				       {
			 
					 group->global->Fill(num);
				       }
		     
				   },
				   StyleKinds::test);
      }


#ifndef RETFUNC
#define RETFUNC(...) [&](const int i){ return double(__VA_ARGS__); }
#endif

    add_plot<joined_plotter_time>("time_histogram", "Event Processing Time", "#font[52]{t} [#mus]", "Fraction of Events",
				  add_plot<H1D_plotter_time>("time_histogram_reference",
							     RETFUNC(std::min(min_reference_time[0], min_modified_time[0])),
							     RETFUNC(std::max(max_reference_time[0], max_modified_time[0])),
							     false, 0, ref_name + " Event Processing Time", "#font[52]{t} [#mus]", (normalize ? "Fraction of Events" : "Events"),
							     [&](hist_group_1D *group)
							     {
							       for (const auto num : reference_time_columns[0])
								 {
								   group->global->Fill(num);
								 }
							     },
							     StyleKinds::ref),
				  ref_name,
				  add_plot<H1D_plotter_time>("time_histogram_modified",
							     RETFUNC(std::min(min_modified_time[0], min_modified_time[0])),
							     RETFUNC(std::max(max_modified_time[0], max_modified_time[0])),
							     false, 0, test_name + " Event Processing Time", "#font[52]{t} [#mus]", (normalize ? "Fraction of Events" : "Events"),
							     [&](hist_group_1D *group)
							     {
							       for (const auto num : modified_time_columns[0])
								 {
								   group->global->Fill(num);
								 }
							     },
							     StyleKinds::test),
				  test_name);

    
    add_plot<H1D_plotter_time>("time_ratio_histogram",
			       RETFUNC(0),
			       RETFUNC(1),
			       false, 0, "Ratio Between GPU and CPU Event Processing Time", "#font[52]{t}^{(" + test_name + ")}/#font[52]{t}^{(" + ref_name + ")}", (normalize ? "Fraction of Events" : "Events"),
			       [&](hist_group_1D *group)
			       {
				 for (int i = 0; i < reference_time_columns[0].size(); ++i)
				   {
				     group->global->Fill(modified_time_columns[0][i]/reference_time_columns[0][i]);
				   }		     
			       },
			       StyleKinds::joined);

    
    add_plot<H1D_plotter_time>("time_invratio_histogram",
			       RETFUNC(0),
			       RETFUNC(50),
			       false, 0, "GPU Speed-Up of Event Processing Time in Relation to the CPU", "Speed-Up #(){#font[52]{t}^{(" + ref_name + ")}/#font[52]{t}^{(" + test_name + ")}}", (normalize ? "Fraction of Events" : "Events"),
			       [&](hist_group_1D *group)
			       {
				 for (int i = 0; i < reference_time_columns[0].size(); ++i)
				   {
				     group->global->Fill(reference_time_columns[0][i]/modified_time_columns[0][i]);
				   }		     
			       },
			       StyleKinds::joined);
    
    add_plot<H1D_plotter_time>("time_calc_gpu",
			       RETFUNC(min_modified_time[3] + min_modified_time[4] + min_modified_time[5] + min_modified_time[6]),
			       RETFUNC(max_modified_time[3] + max_modified_time[4] + max_modified_time[5] + max_modified_time[6]),
			       false, 0, "Algorithm Execution Time", "t [#mus]", (normalize ? "Fraction of Events" : "Events"),
			       [&](hist_group_1D *group)
			       {
				 for (int i = 0; i < modified_time_columns[0].size(); ++i)
				   {
				     const double num = modified_time_columns[3][i] +
				       modified_time_columns[4][i] +
				       modified_time_columns[5][i] +
				       modified_time_columns[6][i];
				     group->global->Fill(num);
				   }		     
			       },
			       StyleKinds::joined);


  }



};

template <class PlotT>
void plot_together_helper(const std::string& name, PlotT *p)
{
}

template <class PlotT>
void plot_together_helper(const std::string& name, PlotT *p, const ClusterPlotter* plotter)
{
  const auto it = plotter->plots.find(name);
  p->add_plot(it->second.get(), plotter->plotter_name);
}


template <class PlotT>
void plot_together_helper(const std::string& name, PlotT *p, const ClusterPlotter& plotter)
{
  const auto it = plotter.plots.find(name);
  p->add_plot(it->second.get(), plotter.plotter_name);
}

template <class PlotT, class ... Rest>
void plot_together_helper(const std::string& name, PlotT *p, const ClusterPlotter& plotter, Rest && ... rest)
{
  plot_together_helper(name, p, plotter);
  plot_together_helper(name, p, std::forward<Rest>(rest)...);
}

template <class PlotT, class ... Rest>
void plot_together_helper(const std::string& name, PlotT *p, const ClusterPlotter* plotter, Rest && ... rest)
{
  plot_together_helper(name, p, plotter);
  plot_together_helper(name, p, std::forward<Rest>(rest)...);
}


template <class ... Rest>
std::unique_ptr<ClusterPlotter::plotter_base> plot_together(const std::string &name, const ClusterPlotter& first, Rest && ... rest)
{
  const auto it = first.plots.find(name);
  if (it == first.plots.end())
    {
      std::cout << "ERROR: Trying to plot non-existent plot: '" << name << "'" << std::endl;
      return nullptr;
    }
  ClusterPlotter::plotter_base *pplt = it->second.get();
  
  switch(pplt->plotter_kind())
    {
    case ClusterPlotter::PlotterKind::type:
      {
	auto ret = std::make_unique<ClusterPlotter::joined_plotter_type>(&first, pplt->hist_name, pplt->hist_title, pplt->x_label, pplt->y_label);
	ret->add_plot(pplt, first.plotter_name);
	plot_together_helper(ret, std::forward<Rest>(rest)...);
	return std::move(ret);
      }
    case ClusterPlotter::PlotterKind::region:
      {
	auto ret = std::make_unique<ClusterPlotter::joined_plotter_region>(&first, pplt->hist_name, pplt->hist_title, pplt->x_label, pplt->y_label);
	ret->add_plot(pplt, first.plotter_name);
	plot_together_helper(name, ret, std::forward<Rest>(rest)...);
	return std::move(ret);
      }
    case ClusterPlotter::PlotterKind::time:
      {
	auto ret = std::make_unique<ClusterPlotter::joined_plotter_time>(&first, pplt->hist_name, pplt->hist_title, pplt->x_label, pplt->y_label);
	ret->add_plot(pplt, first.plotter_name);
	plot_together_helper(name, ret, std::forward<Rest>(rest)...);
	return std::move(ret);
      }
    default:
      std::cout << "ERROR: Unsupported kind of plotter for joint plotting: " << int(pplt->plotter_kind()) << "." << std::endl;
      return nullptr;
    }
}

std::unique_ptr<ClusterPlotter::plotter_base> plot_together(const std::string &name, const std::vector<const ClusterPlotter *>& plots)
{
  if (plots.size() == 0)
    {
      std::cout << "ERROR: Trying to plot with empty vector of ClustPlotters!" << std::endl;
      return nullptr;
    }
  const auto it = plots[0]->plots.find(name);
  if (it == plots[0]->plots.end())
    {
      std::cout << "ERROR: Trying to plot non-existent plot: '" << name << "'" << std::endl;
      return nullptr;
    }
  ClusterPlotter::plotter_base *pplt = it->second.get();

  switch(pplt->plotter_kind())
    {
    case ClusterPlotter::PlotterKind::type:
      {
	auto ret = std::make_unique<ClusterPlotter::joined_plotter_type>(plots[0], pplt->hist_name, pplt->hist_title, pplt->x_label, pplt->y_label);
	ret->add_plot(pplt, plots[0]->plotter_name);
	for (int i = 1; i < plots.size(); ++i)
	  {
	    const auto it2 = plots[i]->plots.find(name);
	    ret->add_plot(it2->second.get(), plots[i]->plotter_name);
	  }
	return std::move(ret);
      }
    case ClusterPlotter::PlotterKind::region:
      {
	auto ret = std::make_unique<ClusterPlotter::joined_plotter_region>(plots[0], pplt->hist_name, pplt->hist_title, pplt->x_label, pplt->y_label);
	ret->add_plot(pplt, plots[0]->plotter_name);
	for (int i = 1; i < plots.size(); ++i)
	  {
	    const auto it2 = plots[i]->plots.find(name);
	    ret->add_plot(it2->second.get(), plots[i]->plotter_name);
	  }
	return std::move(ret);
      }
    case ClusterPlotter::PlotterKind::time:
      {
	auto ret = std::make_unique<ClusterPlotter::joined_plotter_time>(plots[0], pplt->hist_name, pplt->hist_title, pplt->x_label, pplt->y_label);
	ret->add_plot(pplt, plots[0]->plotter_name);
	for (int i = 1; i < plots.size(); ++i)
	  {
	    const auto it2 = plots[i]->plots.find(name);
	    ret->add_plot(it2->second.get(), plots[i]->plotter_name);
	  }
	return std::move(ret);
      }
    default:
      std::cout << "ERROR: Unsupported kind of plotter for joint plotting: " << int(pplt->plotter_kind()) << "." << std::endl;
      return nullptr;
    }
}

struct TogetherPlot
{
  std::vector<const ClusterPlotter *> plots;


  void add_plot(const ClusterPlotter *cp)
  {
    //std::cout << "Adding " << cp->plotter_name << std::endl;
    plots.push_back(cp);
  }

  void add_plot()
  {
  }

  template <class ... Rest>
  void add_plot(const ClusterPlotter *cp, Rest && ... rest)
  {
    add_plot(cp);
    add_plot(std::forward<Rest>(rest)...);
  }

  template<class ... Args>
  TogetherPlot(Args && ... args)
  {
    add_plot(std::forward<Args>(args)...);
  }

  
  template <class ... Args>
  void plot(const std::string & name, Args && ... args) const
  {
    std::unique_ptr<ClusterPlotter::plotter_base> plt(plot_together(name, plots));

    if (plt)
      {
	plt->plot(std::forward<Args>(args)...);
      }
  }
};


#include "ClusterPlotterPlots.h"

#endif
